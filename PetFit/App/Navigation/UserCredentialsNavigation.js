import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {LOGIN_PAGE} from '../Constants/PageNames';

import LoginPage from '../UIScreens/LoginPage/LoginPage';
// import RegisterPage from '../UIScreens/RegisterPage/RegisterPage';

const Stack = createStackNavigator();

const UserCredentialsNavigator = () => {
  const userCredentialsNavigator = (
    <Stack.Navigator initialRouteName={LOGIN_PAGE} headerMode="None">
      <Stack.Screen name={LOGIN_PAGE} component={LoginPage} />
    </Stack.Navigator>
  );
  return userCredentialsNavigator;
};

export default UserCredentialsNavigator;

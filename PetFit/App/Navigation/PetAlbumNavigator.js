import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {PET_PICTURES_PAGE, PET_VIDEOS_PAGE} from './../Constants/PageNames';
import PetPicturesPage from '../UIScreens/MyPets/PetAlbumPage/PetPicturesPage';
import PetVideosPage from '../UIScreens/MyPets/PetAlbumPage/PetVideosPage';

const Tab = createMaterialTopTabNavigator();

const PetAlbumNavigator = (props) => {
  const {route} = props;
  const petDetails = route.params.petDetails;
  return (
    <></>
    // <Tab.Navigator>
    //   <Tab.Screen
    //     name={PET_PICTURES_PAGE}
    //     component={() => PetPicturesPage(props)}
    //   />
    //   <Tab.Screen
    //     name={PET_VIDEOS_PAGE}
    //     component={() => PetPicturesPage(props)}
    //   />
    // </Tab.Navigator>
    // <Tab.Navigator>
    //   <Tab.Screen
    //     name={PET_PICTURES_PAGE}
    //     component={PetPicturesPage}
    //   />
    //   <Tab.Screen
    //     name={PET_VIDEOS_PAGE}
    //     component={PetPicturesPage}
    //   />
    // </Tab.Navigator>
  );
};

export default PetAlbumNavigator;

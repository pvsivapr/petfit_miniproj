import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AppDrawerNavigator from './DrawerNavigation';
import IntroPage from './../UIScreens/IntroPage/IntroPage';

import {
  INTRO_PAGE,
  DRAWER_NAVIGATOR,
  USER_CREDENTIALS_NAVIGATOR,
  SUCCESS,
  FAILURE,
} from '../Constants/PageNames';

import UserCredentialsNavigator from './UserCredentialsNavigation';
import {useEffect} from 'react';

import {connect} from 'react-redux';

import {
  fetchGetUserCredentialsAccess,
  fetchGetUserCredentialsReset,
} from './../BAL/CommonReducers/CommonActions/GetUserCredentialsActions';
import {ConsoleLogger} from '../BAL/EventLogger';

const Stack = createStackNavigator();

const Root_Navigator = (props) => {
  const {dispatch, serviceState, userData} = props;
  useEffect(() => {
    dispatch(fetchGetUserCredentialsAccess());
  }, []);

  const rootNavigatorUser = (
    <Stack.Navigator initialRouteName={INTRO_PAGE} headerMode="None">
      <Stack.Screen name={INTRO_PAGE} component={IntroPage} />
      <Stack.Screen name={DRAWER_NAVIGATOR} component={AppDrawerNavigator} />
      <Stack.Screen
        name={USER_CREDENTIALS_NAVIGATOR}
        component={UserCredentialsNavigator}
      />
    </Stack.Navigator>
  );

  const rootNavigatorExistingUser = (
    <Stack.Navigator initialRouteName={DRAWER_NAVIGATOR} headerMode="None">
      <Stack.Screen name={INTRO_PAGE} component={IntroPage} />
      <Stack.Screen name={DRAWER_NAVIGATOR} component={AppDrawerNavigator} />
      <Stack.Screen
        name={USER_CREDENTIALS_NAVIGATOR}
        component={UserCredentialsNavigator}
      />
    </Stack.Navigator>
  );

  const rootNavigatorNewUser = (
    <Stack.Navigator
      initialRouteName={USER_CREDENTIALS_NAVIGATOR}
      headerMode="None">
      <Stack.Screen name={INTRO_PAGE} component={IntroPage} />
      <Stack.Screen
        name={USER_CREDENTIALS_NAVIGATOR}
        component={UserCredentialsNavigator}
      />
      <Stack.Screen name={DRAWER_NAVIGATOR} component={AppDrawerNavigator} />
    </Stack.Navigator>
  );

  let rootNavigator = rootNavigatorUser;

  // if (serviceState === SUCCESS) {
  //   if (userData !== null && userData !== undefined && userData !== {}) {
  //     if (
  //       userData.name !== null &&
  //       userData.name !== undefined &&
  //       userData.name !== ''
  //     ) {
  //       rootNavigator = rootNavigatorExistingUser;
  //     } else {
  //       rootNavigator = rootNavigatorNewUser;
  //     }
  //   } else {
  //     rootNavigator = rootNavigatorNewUser;
  //   }
  // } else if (serviceState === FAILURE) {
  //   rootNavigator = rootNavigatorNewUser;
  // } else {}

  return rootNavigator;
};

const mapStatetoProps = (state) => {
  return {
    userData: state.GetUserCredentialsReducer.userData,
    serviceState: state.GetUserCredentialsReducer.serviceState,
    loaderVisibility: state.GetUserCredentialsReducer.loaderVisibility,
  };
};

const RootNavigator = connect(mapStatetoProps)(Root_Navigator);
export default RootNavigator;

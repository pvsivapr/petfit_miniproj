import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {
  HOME_PAGE,
  PET_DETAIL_PAGE,
  SCHEDULE_PET_ACTIVITY_PAGE,
  VIEW_PET_ACTIVITIES_PAGE,
  PET_ALBUM_PAGE,
  // PET_ALBUM_NAVIGATOR,
  PET_PICTURES_PAGE,
} from '../Constants/PageNames';
import HomePage from '../UIScreens/HomePage/HomePage';
import PetDetailPage from './../UIScreens/MyPets/PetDetailPage/PetDetailPage';
import SchedulePetActivitiesPage from './../UIScreens/MyPets/SchedulePetActivitiesPage/SchedulePetActivitiesPage';
import ViewPetActivitiesPage from './../UIScreens/MyPets/ViewPetActivitiesPage/ViewPetActivitiesPage';
// import PetAlbumNavigator from './PetAlbumNavigator';
import PetPicturesPage from './../UIScreens/MyPets/PetAlbumPage/PetPicturesPage';
import PetAlbumPage from './../UIScreens/MyPets/PetAlbumPage/PetAlbumPage';

const Stack = createStackNavigator();

const HomeNavigator = () => {
  const homeNavigator = (
    <Stack.Navigator initialRouteName={HOME_PAGE} headerMode={'none'}>
      <Stack.Screen name={HOME_PAGE} component={HomePage} />
      <Stack.Screen name={PET_DETAIL_PAGE} component={PetDetailPage} />
      <Stack.Screen
        name={SCHEDULE_PET_ACTIVITY_PAGE}
        component={SchedulePetActivitiesPage}
      />
      <Stack.Screen
        name={VIEW_PET_ACTIVITIES_PAGE}
        component={ViewPetActivitiesPage}
      />
      {/* <Stack.Screen name={PET_ALBUM_NAVIGATOR} component={PetAlbumNavigator} /> */}
      <Stack.Screen name={PET_PICTURES_PAGE} component={PetPicturesPage} />
      <Stack.Screen name={PET_ALBUM_PAGE} component={PetAlbumPage} />
    </Stack.Navigator>
  );
  return homeNavigator;
};
export default HomeNavigator;

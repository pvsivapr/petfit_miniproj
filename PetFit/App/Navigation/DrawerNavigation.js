import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {
  HOME_NAVIGATOR,
  ADD_PETS_PAGE,
  USER_CREDENTIALS_NAVIGATOR,
  LOGOUT_USER,
  ADD_PRODUCT_PAGE,
  VIEW_PRODUCT_PAGE,
  ADD_FEEDBACK_PAGE,
  VIEW_FEEDBACK_PAGE,
} from '../Constants/PageNames';

import HomeNavigator from './HomeNavigator';
import AddPetPage from '../UIScreens/MyPets/AddPetPage/AddPetPage';
import CustomTouch from '../CommonComponents/CustomTouch';

import {USER_PROFILE_DETAILS} from './../BAL/LocalDB/LocalDBConstants';
import {CustomAsyncStorage} from '../BAL/LocalDB';
import AddProductPage from '../UIScreens/Products/AddProductPage/AddProductPage';
import ViewProductsPage from '../UIScreens/Products/ViewProductsPage/ViewProductsPage';
import AddFeedbackPage from '../UIScreens/Feedback/AddFeedbackPage/AddFeedbackPage';
import ViewFeedbackPage from '../UIScreens/Feedback/ViewFeedbackPage/ViewFeedbackPage';
import {connect} from 'react-redux';

class SidebarSection extends React.Component {
  render() {
    const {listItems, isAdmin} = this.props;
    return (
      <SafeAreaView>
        <ScrollView>
          <View>
            {isAdmin === true ? (
              <></>
            ) : (
              <Image
                source={{uri: 'http://lorempixel.com/400/200/abstract/'}}
                style={{height: 200}}
              />
            )}
            <View style={styles.listViewItemsHolderStyle}>
              {listItems.map((item) => {
                let listViewItem_HolderStyle = styles.listViewItemHolderStyle;
                if (item.title === 'Logout') {
                  listViewItem_HolderStyle = {
                    ...listViewItem_HolderStyle,
                    marginTop: isAdmin === true ? 5 : 20,
                  };
                }
                return (
                  <View style={listViewItem_HolderStyle}>
                    <CustomTouch
                      onPress={() => {
                        if (item.navigationId !== LOGOUT_USER) {
                          if (item.navigationId !== '') {
                            this.props.navigation.navigate(item.navigationId);
                          }
                        } else {
                          CustomAsyncStorage.addDataToStorage(
                            USER_PROFILE_DETAILS,
                            null,
                          );
                          this.props.navigation.navigate(
                            USER_CREDENTIALS_NAVIGATOR,
                          );
                        }
                      }}>
                      <Text style={styles.listItemButtonTextStyle}>
                        {item.title}
                      </Text>
                    </CustomTouch>
                  </View>
                );
              })}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const Drawer = createDrawerNavigator();
const AppDrawer_Navigator = (props) => {
  const {route} = props;

  let isAdmin = route.params.isAdmin;

  let listItems = [];

  const userListItems = [
    {
      title: 'Home',
      navigationId: HOME_NAVIGATOR,
    },
    {
      title: 'Add Pet',
      navigationId: ADD_PETS_PAGE,
    },
    {
      title: 'Help',
      navigationId: '',
    },
    {
      title: 'Support',
      navigationId: ADD_FEEDBACK_PAGE,
    },
    {
      title: 'About',
      navigationId: '',
    },
    {
      title: 'Logout',
      navigationId: LOGOUT_USER,
    },
  ];

  const adminListItems = [
    {
      title: 'All Products',
      navigationId: VIEW_PRODUCT_PAGE,
    },
    {
      title: 'Add Products',
      navigationId: ADD_PRODUCT_PAGE,
    },
    {
      title: 'View Feedback',
      navigationId: VIEW_FEEDBACK_PAGE,
    },
    {
      title: 'Logout',
      navigationId: LOGOUT_USER,
    },
  ];
  if (isAdmin === true) {
    listItems = adminListItems;
    isAdmin = true;
  } else {
    listItems = userListItems;
  }

  const appDrawerNavigator = (
    <Drawer.Navigator
      drawerContent={(props) => (
        <SidebarSection {...props} listItems={listItems} isAdmin={isAdmin} />
      )}>
      {isAdmin === false ? (
        <Drawer.Screen name={HOME_NAVIGATOR} component={HomeNavigator} />
      ) : (
        <Drawer.Screen name={VIEW_PRODUCT_PAGE} component={ViewProductsPage} />
      )}
      <Drawer.Screen name={ADD_PETS_PAGE} component={AddPetPage} />
      <Drawer.Screen name={ADD_PRODUCT_PAGE} component={AddProductPage} />
      <Drawer.Screen name={ADD_FEEDBACK_PAGE} component={AddFeedbackPage} />
      <Drawer.Screen name={VIEW_FEEDBACK_PAGE} component={ViewFeedbackPage} />
    </Drawer.Navigator>
  );
  return appDrawerNavigator;
};

const mapStateToProps = (state) => {
  return {
    userDetails: state.GetUserCredentialsReducer.userDetails,
    serviceState: state.GetUserCredentialsReducer.serviceState,
  };
};

const AppDrawerNavigator = connect(mapStateToProps)(AppDrawer_Navigator);
export default AppDrawerNavigator;

const styles = StyleSheet.create({
  listItemButtonTextStyle: {
    color: 'rgb(198,198,198)',
    marginLeft: 30,
    marginVertical: 15,
  },
  listViewItemHolderStyle: {
    backgroundColor: 'rgb(84,84,84)',
    marginVertical: 5,
  },
  listViewItemsHolderStyle: {
    backgroundColor: 'rgb(168,170,170)',
    marginTop: 30,
  },
});

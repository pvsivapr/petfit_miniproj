import {ConsoleLogger, ErrorEventLogger} from '../../EventLogger';
import {CustomAsyncStorage} from '../../LocalDB';
import {USER_PROFILE_DETAILS} from '../../LocalDB/LocalDBConstants';

export const GET_USER_CREDENTIALS_ACCESS = 'GET_USER_CREDENTIALS_ACCESS';
export const GET_USER_CREDENTIALS_SUCCESS = 'GET_USER_CREDENTIALS_SUCCESS';
export const GET_USER_CREDENTIALS_FAILURE = 'GET_USER_CREDENTIALS_FAILURE';
export const GET_USER_CREDENTIALS_RESET = 'GET_USER_CREDENTIALS_RESET';

export const getUserCredentialsAccess = () => ({
  type: GET_USER_CREDENTIALS_ACCESS,
});

export const getUserCredentialsSuccess = (user_Data) => ({
  type: GET_USER_CREDENTIALS_SUCCESS,
  payload: user_Data,
});

export const getUserCredentialsFailure = () => ({
  type: GET_USER_CREDENTIALS_FAILURE,
});

export const fetchGetUserCredentialsReset = () => ({
  type: GET_USER_CREDENTIALS_RESET,
});

export function fetchGetUserCredentialsAccess() {
  return async (dispatchUserCredentials) => {
    dispatchUserCredentials(getUserCredentialsAccess());
    try {
      CustomAsyncStorage.getDataFromStorage(
        USER_PROFILE_DETAILS,
        (successData) => {
          const user_Data = JSON.parse(successData.response_data);
          // ConsoleLogger(
          //   'GetUserCredentialsActions >>>> fetchGetUserCredentialsAccess >>>> CustomAsyncStorage.getDataFromStorage >>>> user_Data: ',
          //   user_Data,
          // );
          dispatchUserCredentials(getUserCredentialsSuccess(user_Data));
        },
        (failureData) => {
          dispatchUserCredentials(getUserCredentialsFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchUserCredentials(
        getUserCredentialsFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

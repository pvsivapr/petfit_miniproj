import {
  GET_USER_CREDENTIALS_ACCESS,
  GET_USER_CREDENTIALS_SUCCESS,
  GET_USER_CREDENTIALS_FAILURE,
} from './CommonActions/GetUserCredentialsActions';
import {FAILURE, INACTIVE, LOADING, SUCCESS} from './../../Constants/PageNames';
import { ConsoleLogger } from '../EventLogger';

const initialState = {
  userDetails: null,
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const GetUserCredentialsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_CREDENTIALS_ACCESS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case GET_USER_CREDENTIALS_SUCCESS:
      ConsoleLogger(
        'GetUserCredentialsReducer >>>> GET_USER_CREDENTIALS_SUCCESS >>>> action.payload: ',
        action.payload,
      );
      state = {
        ...state,
        serviceState: SUCCESS,
        loaderVisibility: false,
        userDetails: action.payload,
        successMessage: action.payload,
      };
      break;
    case GET_USER_CREDENTIALS_FAILURE:
      state = {
        ...state,
        loaderVisibility: false,
        serviceState: FAILURE,
        errorMessage: action.payload,
      };
      break;
  }
  return state;
};

export default GetUserCredentialsReducer;

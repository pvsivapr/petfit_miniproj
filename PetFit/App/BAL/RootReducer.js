import {combineReducers} from 'redux';
import UserLoginReducer from './../UIScreens/LoginPage/LoginReducer';
import UserRegisterReducer from './../UIScreens/RegisterPage/RegisterReducer';
import AddPetReducer from './../UIScreens/MyPets/AddPetPage/AddPetReducer';
import HomeReducer from './../UIScreens/HomePage/HomeReducer';
import PetDetailReducer from './../UIScreens/MyPets/PetDetailPage/PetDetailReducer';
import SchedulePetActivitiesReducer from './../UIScreens/MyPets/SchedulePetActivitiesPage/SchedulePetActivitiesReducer';
import ViewPetActivitiesReducer from './../UIScreens/MyPets/ViewPetActivitiesPage/ViewPetActivitiesReducer';
import AddProductReducer from './../UIScreens/Products/AddProductPage/AddProductReducer';
import ViewProductsReducer from './../UIScreens/Products/ViewProductsPage/ViewProductsReducer';
import ViewFeedbackReducer from './../UIScreens/Feedback/ViewFeedbackPage/ViewFeedbackReducer';
import AddFeedbackReducer from './../UIScreens/Feedback/AddFeedbackPage/AddFeedbackReducer';
import GetUserCredentialsReducer from './CommonReducers/GetUserCredentialsReducer';

export const RootReducer = combineReducers({
  UserLoginReducer,
  UserRegisterReducer,
  AddPetReducer,
  HomeReducer,
  PetDetailReducer,
  SchedulePetActivitiesReducer,
  ViewPetActivitiesReducer,
  AddProductReducer,
  ViewProductsReducer,
  ViewFeedbackReducer,
  AddFeedbackReducer,
  GetUserCredentialsReducer,
});

import React from 'react';
// import { openDatabase } from "react-native-sqlite-storage"

import {
  FAILURE_STATUS_CODE,
  NOT_FOUND_STATUS_CODE,
  SUCCESS_STATUS_CODE,
} from './LocalDBConstants';
import {CreateReponseObject} from './../DataHelper';
import {ConsoleLogger, ErrorEventLogger} from '../EventLogger';
import AsyncStorage from '@react-native-community/async-storage';

const title = 'LocalStorage';

export const CustomAsyncStorage = {
  addDataToStorage: async function (
    dataKey,
    dataValue = null,
    successCallBackFunction = null,
    failureCallBackFunction = null,
  ) {
    try {
      const response = await AsyncStorage.setItem(dataKey, dataValue);
      successCallBackFunction();
      ConsoleLogger(`${title} >>>> addDataToStorage >>>> response :`, response);
    } catch (error) {
      // Error saving data
      ErrorEventLogger(title, error);
      const statusMessage = `${title} >>>> addDataToStorage >>>> catch >>>> error :`;
      var responseObj = CreateReponseObject(
        FAILURE_STATUS_CODE,
        statusMessage,
        {},
      );
      if (failureCallBackFunction !== null) {
        failureCallBackFunction(responseObj);
      }
    }
  },

  getDataFromStorage: async function (
    dataKey,
    successCallBackFunction = null,
    failureCallBackFunction = null,
  ) {
    try {
      // ConsoleLogger(`${title} >>>> getDataFromStorage >>>> dataKey :`, dataKey);
      const response = await AsyncStorage.getItem(dataKey);
      if (response !== null) {
        // ConsoleLogger(
        //   `${title} >>>> getDataFromStorage >>>> response :`,
        //   response,
        // );
        const statusMessage = '';
        var response_Obj = CreateReponseObject(
          SUCCESS_STATUS_CODE,
          statusMessage,
          response,
        );
        // ConsoleLogger(
        //   `${title} >>>> getDataFromStorage >>>> response_Obj :`,
        //   response_Obj,
        // );
        if (successCallBackFunction !== null) {
          successCallBackFunction(response_Obj);
        }
        return response_Obj;
      } else {
        ErrorEventLogger(
          `Error >>>> ${title} >>>> getDataFromStorage >>>> dataKey :`,
          dataKey,
        );
        // failureCallBackFunction(responseObj);
        const statusMessage = '';
        var response_Obj = CreateReponseObject(
          SUCCESS_STATUS_CODE,
          statusMessage,
          response,
        );
        if (successCallBackFunction !== null) {
          successCallBackFunction(response_Obj);
        }
        return response_Obj;
      }
    } catch (error) {
      ErrorEventLogger(title, error);
      const statusMessage = `${title} >>>> getDataFromStorage >>>> catch >>>> error :`;
      var responseObj = CreateReponseObject(
        FAILURE_STATUS_CODE,
        statusMessage,
        {},
      );
      if (failureCallBackFunction !== null) {
        // ErrorEventLogger(title, error);
        failureCallBackFunction(responseObj);
      }
      return response_Obj;
    }
  },
};

// export const databaseConnection = openDatabase({ name: 'UserExpensesDatabase.db' });
export const ExecuteDBOperations = {
  // createTable: function (tableName, tableContentsObject = [], successCallBackFunction = null, failureCallBackFunction = null) {
  //     let queryString = "CREATE TABLE if not exists " + tableName + " (";
  //     tableContentsObject.forEach(item => {
  //         var subQueryString = "";
  //         if (item.columnName && item.columnName !== "" && item.columnName !== undefined && item.columnName !== null) {
  //             subQueryString += item.columnName
  //         }
  //         if (item.columnType && item.columnType !== "" && item.columnType !== undefined && item.columnType !== null) {
  //             subQueryString += " " + item.columnType
  //             if (item.maxLength && item.maxLength !== undefined && item.maxLength !== null) {
  //                 subQueryString += "(" + item.maxLength.toString() + ")";
  //             }
  //         }
  //         if (item.isAutoIncrement && item.isAutoIncrement !== undefined && item.isAutoIncrement !== null && item.isAutoIncrement) {
  //             subQueryString += " AUTO INCREMENT";
  //         }
  //         if (item.isPrimary && item.isPrimary !== undefined && item.isPrimary !== null && item.isPrimary) {
  //             subQueryString += " PRIMARY KEY";
  //         }
  //         queryString += subQueryString + ",";
  //     });
  //     const query_String = queryString.slice(0, -1);
  //     queryString = query_String + ");";
  //     ConsoleLogger(title, queryString);
  //     databaseConnection.transaction(function (txn) {
  //         txn.executeSql(queryString, [], (txn, results) => {
  //             const statusMessage = `"${tableName}" table is Created successfully`;
  //             var responseObj = CreateReponseObject(SUCCESS_STATUS_CODE, statusMessage, {});
  //             if (successCallBackFunction !== null) {
  //                 successCallBackFunction(responseObj);
  //             }
  //         }
  //         );
  //     }, (error) => {
  //         ErrorEventLogger(title, error);
  //         const statusMessage = `Unable to create "${tableName}" table`;
  //         var responseObj = CreateReponseObject(FAILURE_STATUS_CODE, statusMessage, {});
  //         if (failureCallBackFunction !== null) {
  //             failureCallBackFunction(responseObj);
  //         }
  //     });
  // },
  // checkForTable: function(tableName, successCallBackFunction = null, failureCallBackFunction = null){
  //     let queryString = `select DISTINCT tbl_name from sqlite_master where tbl_name='${tableName}';`;
  //     ConsoleLogger(title, queryString);
  //     databaseConnection.transaction(function (txn) {
  //         txn.executeSql(queryString, [], (txn, results) => {
  //             ConsoleLogger(title, results);
  //             var len = results.rows.length;
  //             if (len > 0) {
  //                 const res = results.rows.item(0);
  //                 const statusMessage = `"${tableName}" table is exist`;
  //                 var responseObj = CreateReponseObject(SUCCESS_STATUS_CODE, statusMessage, {});
  //                 if (successCallBackFunction !== null) {
  //                     successCallBackFunction(responseObj);
  //                 }
  //             }
  //             else {
  //                 const statusMessage = `"${tableName}" table does not exist`;
  //                 var responseObj = CreateReponseObject(NOT_FOUND_STATUS_CODE, statusMessage, {});
  //                 if (successCallBackFunction !== null) {
  //                     successCallBackFunction(responseObj);
  //                 }
  //             }
  //         }, (error) => {
  //             ErrorEventLogger(title, error);
  //             const statusMessage = `Unknown error found while searching for table : "${tableName}"`;
  //             var responseObj = CreateReponseObject(FAILURE_STATUS_CODE, statusMessage, {});
  //             if (failureCallBackFunction !== null) {
  //                 failureCallBackFunction(responseObj);
  //             }
  //         });
  //     });
  // },
  // dropTable: function (tableName, successCallBackFunction = null, failureCallBackFunction = null) {
  //     let queryString = "DROP TABLE if exists " + tableName;
  //     ConsoleLogger(title, queryString);
  //     databaseConnection.transaction(function (txn) {
  //         txn.executeSql(queryString, [], (txn, results) => {
  //             const statusMessage = `"${tableName}" table is deleted successfully`;
  //             var responseObj = CreateReponseObject(SUCCESS_STATUS_CODE, statusMessage, {});
  //             if (successCallBackFunction !== null) {
  //                 successCallBackFunction(responseObj);
  //             }
  //         }, (error) => {
  //             ErrorEventLogger(title, error);
  //             const statusMessage = `Unable to delete "${tableName}" table`;
  //             var responseObj = CreateReponseObject(FAILURE_STATUS_CODE, statusMessage, {});
  //             if (failureCallBackFunction !== null) {
  //                 failureCallBackFunction(responseObj);
  //             }
  //         });
  //     });
  // },
  // insertWithQuery: function (tableName, query, values = [], successCallBackFunction = null, failureCallBackFunction = null) {
  //     databaseConnection.transaction(function (txn) {
  //         let questionMarkString = "";
  //         values.forEach(value_item => {
  //             questionMarkString += "?,"
  //         })
  //         const questionMark_String = questionMarkString.slice(0, -1);
  //         questionMarkString = questionMark_String + ")";
  //         ConsoleLogger("test", questionMarkString);
  //         let queryString = `INSERT INTO ${tableName} ${query} VALUES (${questionMarkString}`;
  //         ConsoleLogger(title, queryString);
  //         txn.executeSql(queryString, values, (txn, results) => {
  //             ConsoleLogger(title, results);
  //             if (results.rowsAffected > 0) {
  //                 const statusMessage = `Values are inserted in to the "${tableName}"`;
  //                 var responseObj = CreateReponseObject(SUCCESS_STATUS_CODE, statusMessage, {});
  //                 if (successCallBackFunction !== null) {
  //                     successCallBackFunction(responseObj);
  //                 }
  //             }
  //             else {
  //                 const statusMessage = `Unable to add data to "${tableName}"`;
  //                 var responseObj = CreateReponseObject(FAILURE_STATUS_CODE, statusMessage, {});
  //                 if (failureCallBackFunction !== null) {
  //                     failureCallBackFunction(responseObj);
  //                 }
  //             }
  //         }, (error) => {
  //             ErrorEventLogger(title, error);
  //             const statusMessage = `Unable to add data to "${tableName}"`;
  //             var responseObj = CreateReponseObject(FAILURE_STATUS_CODE, statusMessage, {});
  //             if (failureCallBackFunction !== null) {
  //                 failureCallBackFunction(responseObj);
  //             }
  //         });
  //     });
  // },
  // getAvailableDataFromTable: function (tableName, query = "", values = [], successCallBackFunction = null, failureCallBackFunction = null) {
  //     databaseConnection.transaction(function (txn) {
  //         let queryString = "";
  //         if (query !== "") {
  //             queryString = `SELECT * From ${tableName} WHERE ${query}`;
  //         }
  //         else {
  //             queryString = `SELECT * From ${tableName}`;
  //         }
  //         ConsoleLogger(title, queryString);
  //         txn.executeSql(queryString, values, (txn, results) => {
  //             ConsoleLogger(title, results);
  //             var len = results.rows.length;
  //             if (len > 0) {
  //                 const res = results.rows.item(0);
  //                 const data = [];
  //                 for (let i = 0; i < results.rows.length; ++i) {
  //                     data.push(results.rows.item(i));
  //                 };
  //                 const statusMessage = `Values are obtained successfully from "${tableName}"`;
  //                 var responseObj = CreateReponseObject(SUCCESS_STATUS_CODE, statusMessage, data);
  //                 if (successCallBackFunction !== null) {
  //                     successCallBackFunction(responseObj);
  //                 }
  //             }
  //             else {
  //                 const statusMessage = `Data is empty "${tableName}"`;
  //                 var responseObj = CreateReponseObject(NOT_FOUND_STATUS_CODE, statusMessage, {});
  //                 if (successCallBackFunction !== null) {
  //                     successCallBackFunction(responseObj);
  //                 }
  //             }
  //         }, (error) => {
  //             ErrorEventLogger(title, error);
  //             const statusMessage = `Unable to get data from "${tableName}"`;
  //             var responseObj = CreateReponseObject(FAILURE_STATUS_CODE, statusMessage, {});
  //             if (failureCallBackFunction !== null) {
  //                 failureCallBackFunction(responseObj);
  //             }
  //         });
  //     });
  // }
};

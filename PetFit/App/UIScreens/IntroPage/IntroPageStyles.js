import React from 'react';
import {StyleSheet} from 'react-native';
import AppColors from './../../Constants/Colors';

const styles = StyleSheet.create({
  imageViewStyle: {
    height: 150,
    width: 150,
  },
  imageViewHolderStyle: {
    height: 150,
    width: 150,
    margin: 10,
    marginBottom: 10,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: 'green',
    alignSelf: 'center',
  },

  mainViewStyle: {
    // flex: 1,
    backgroundColor: '#FF0000',
    paddingTop: 0,
  },
});

export default styles;

import React from 'react';
import {View, Image, SafeAreaView} from 'react-native';
import styles from './IntroPageStyles';
import {PET_FIT_LOGO} from './../../Assets/ImageHelper';
import CustomActivityIndicator from '../../CommonComponents/CustomActivityIndicator';
import {connect} from 'react-redux';
import {
  DRAWER_NAVIGATOR,
  USER_CREDENTIALS_NAVIGATOR,
} from '../../Constants/PageNames';
import {fetchGetUserCredentialsReset} from './../../BAL/CommonReducers/CommonActions/GetUserCredentialsActions';
import {CustomAsyncStorage} from '../../BAL/LocalDB';
import {USER_PROFILE_DETAILS} from '../../BAL/LocalDB/LocalDBConstants';

class Intro_Page extends React.Component {
  componentDidMount() {
    const {navigation} = this.props;
    CustomAsyncStorage.getDataFromStorage(
      USER_PROFILE_DETAILS,
      (successData) => {
        const userDetails = JSON.parse(successData.response_data);
        if (
          userDetails !== null &&
          userDetails.name !== undefined &&
          userDetails.name !== ''
        ) {
          navigation.navigate(DRAWER_NAVIGATOR, {isAdmin: userDetails.isAdmin});
        } else {
          navigation.navigate(USER_CREDENTIALS_NAVIGATOR);
        }
      },
      () => {
        navigation.navigate(USER_CREDENTIALS_NAVIGATOR);
      },
    );
  }

  componentWillUnmount() {
    const {dispatch} = this.props;
    dispatch(fetchGetUserCredentialsReset());
  }

  render() {
    const {loaderVisibility} = this.props;
    const uiMain = (
      <SafeAreaView>
        <View style={styles.mainViewStyle}>
          <CustomActivityIndicator visibility={loaderVisibility} />
          <View style={styles.imageViewHolderStyle}>
            <Image style={styles.imageViewStyle} source={PET_FIT_LOGO} />
          </View>
        </View>
      </SafeAreaView>
    );
    return uiMain;
  }
}

const mapStatetoProps = (state) => {
  return {
    userDetails: state.GetUserCredentialsReducer.userDetails,
    serviceState: state.GetUserCredentialsReducer.serviceState,
    loaderVisibility: state.GetUserCredentialsReducer.loaderVisibility,
  };
};

const IntroPage = connect(mapStatetoProps)(Intro_Page);
export default IntroPage;

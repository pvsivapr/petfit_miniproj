import {Dimensions, StyleSheet} from 'react-native';
import {
  APP_MIN_INPUT_HEIGHT,
  APP_MIN_INPUT_WIDTH,
} from './../../Constants/PageDimensions';
import {TEXT_INFO_COLOR} from './../../Constants/AppStyleConstants';
const screenWidth = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  textStyle: {
    color: TEXT_INFO_COLOR,
  },
  textLinkStyle: {
    color: 'blue',
  },
  textErrorInfoStyle: {
    color: 'maroon',
  },
  textInputHolderViewStyle: {
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  imageViewHolderStyle: {
    marginLeft: (screenWidth - 150) / 2,
    marginRight: (screenWidth - 150) / 2,
    marginBottom: 30,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: 'green',
    paddingBottom: 0,
  },
  imageViewStyle: {
    height: 150,
    width: 150,
  },
  buttonSendStyle: {
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
    marginBottom: 10,
  },
  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgb(170, 170, 170)',
  },
});

export default styles;

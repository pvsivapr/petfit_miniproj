import {
  CHECK_REPORT_LOGIN_FORM_ERROR,
  UPDATE_USER_LOGIN_CREDENTIALS,
  USER_LOGIN_ACCESS,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  RESET_LOGIN_FORM,
} from './LoginActions';
import {FAILURE, INACTIVE, LOADING, SUCCESS} from './../../Constants/PageNames';

const initialState = {
  userDetails: {
    name: '',
    password: '',
    isAdmin: false,
  },
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const UserLoginReducer = (state = initialState, action) => {
  state = {
    ...state,
    userDetails: {
      name: '',
      password: '',
      isAdmin: false,
    },
    serviceState: INACTIVE,
    loaderVisibility: false,
    successMessage: '',
    errorMessage: '',
    formErrorMessage: '',
  };
  switch (action.type) {
    case CHECK_REPORT_LOGIN_FORM_ERROR:
      state = {...state, formErrorMessage: action.payload};
      break;
    case UPDATE_USER_LOGIN_CREDENTIALS:
      state = {...state, userDetails: action.payload};
      break;
    case USER_LOGIN_ACCESS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case USER_LOGIN_SUCCESS:
      state = {
        ...state,
        serviceState: SUCCESS,
        loaderVisibility: false,
        successMessage: action.payload,
      };
      break;
    case USER_LOGIN_FAILURE:
      state = {
        ...state,
        loaderVisibility: false,
        serviceState: FAILURE,
        errorMessage: action.payload,
      };
      break;
    case RESET_LOGIN_FORM:
      state = {...state};
      break;
  }
  return state;
};

export default UserLoginReducer;

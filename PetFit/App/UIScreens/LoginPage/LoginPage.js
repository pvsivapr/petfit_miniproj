import React from 'react';
import {View, Text, Platform, KeyboardAvoidingView, Image} from 'react-native';
import {connect} from 'react-redux';
import styles from './LoginPageStyles';
import CustomButton from './../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../CommonComponents/CustomActivityIndicator';
import CustomTextInputField from './../../CommonComponents/CustomTextInputField';
import {DRAWER_NAVIGATOR, SUCCESS} from './../../Constants/PageNames';
import {UserCredentialConstants} from './../../Constants/PageNames';
import {UserCredentialTextConstants} from './../../Constants/TextConstants';

import {
  fetchResetUserLoginDetails,
  fetchCheckReportLoginFormError,
  fetchUpadteLoginUserDetails,
  fetchUserLoginAction,
} from './LoginActions';
import {PET_FIT_LOGO} from './../../Assets/ImageHelper';
import {CustomAsyncStorage} from '../../BAL/LocalDB';
import {USER_PROFILE_DETAILS} from '../../BAL/LocalDB/LocalDBConstants';

class Login_Page extends React.Component {
  async componentDidMount() {
    const {dispatch} = this.props;
    dispatch(fetchResetUserLoginDetails());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.serviceState === SUCCESS) {
      const {navigation} = this.props;
      if (navigation !== null && navigation !== undefined) {
        CustomAsyncStorage.getDataFromStorage(
          USER_PROFILE_DETAILS,
          (successData) => {
            const userDetails = JSON.parse(successData.response_data);
            if (
              userDetails !== null &&
              userDetails.name !== undefined &&
              userDetails.name !== ''
            ) {
              navigation.navigate(DRAWER_NAVIGATOR, {
                isAdmin: userDetails.isAdmin,
              });
            } else {
            }
          },
          () => {},
        );
      }
    }
  }

  onPressUserLogin = () => {
    this.onPressLogin(false);
  };

  onPressAdminLogin = () => {
    this.onPressLogin(true);
  };

  onPressLogin = async (isAdmin) => {
    const {userDetails, dispatch} = this.props;
    if (userDetails !== null && userDetails !== undefined) {
      if (
        userDetails.name === null ||
        userDetails.name === undefined ||
        userDetails.name === ''
      ) {
        dispatch(
          fetchCheckReportLoginFormError('Please enter a valid user name'),
        );
      } else if (
        userDetails.password === null ||
        userDetails.password === undefined ||
        userDetails.password === ''
      ) {
        dispatch(
          fetchCheckReportLoginFormError('Please enter a valid password'),
        );
      } else {
        let user_Details = userDetails;
        if (isAdmin) {
          user_Details = {...userDetails, isAdmin: true};
        }
        dispatch(fetchUserLoginAction(user_Details));
      }
    } else {
      dispatch(
        fetchCheckReportLoginFormError('Please fill all mandatory fields'),
      );
    }
  };
  onTextChangedHandler = (text, inputID) => {
    const {userDetails, dispatch} = this.props;
    let user_Details = {};
    let isupdated = false;
    if (inputID === UserCredentialConstants.USER_NAME) {
      user_Details = {...userDetails, name: text};
      isupdated = true;
    } else if (inputID === UserCredentialConstants.USER_PASSWORD) {
      user_Details = {...userDetails, password: text};
      isupdated = true;
    } else {
    }
    if (isupdated) {
      dispatch(fetchUpadteLoginUserDetails(user_Details));
    }
  };

  render() {
    const {loaderVisibility, userDetails, formErrorMessage} = this.props;
    const uiMain = (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.mainViewStyle}>
        <View style={styles.mainViewStyle}>
          <CustomActivityIndicator visibility={loaderVisibility} />
          <View style={styles.imageViewHolderStyle}>
            <Image style={styles.imageViewStyle} source={PET_FIT_LOGO} />
          </View>
          <View style={styles.textInputHolderViewStyle}>
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_NAME}
              placeHolder={
                UserCredentialTextConstants.USER_NAME_PLACEHOLDER_TEXT
              }
              legendTitle={UserCredentialTextConstants.USER_NAME_LEGEND_TEXT}
              hintText={UserCredentialTextConstants.USER_NAME_ERROR_HINT_TEXT}
              value={userDetails.name}
            />
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_PASSWORD}
              placeHolder={
                UserCredentialTextConstants.USER_PASSWORD_PLACEHOLDER_TEXT
              }
              legendTitle={
                UserCredentialTextConstants.USER_PASSWORD_LEGEND_TEXT
              }
              hintText={
                UserCredentialTextConstants.USER_PASSWORD_ERROR_HINT_TEXT
              }
              value={userDetails.password}
              secureTextEntry={true}
            />
            {formErrorMessage !== null &&
            formErrorMessage !== undefined &&
            formErrorMessage !== '' ? (
              <Text style={styles.textErrorInfoStyle}>{formErrorMessage}</Text>
            ) : (
              <></>
            )}
            <CustomButton
              title={'Login'}
              style={styles.buttonSendStyle}
              onPress={this.onPressLogin}
            />
            <CustomButton
              title={'Login As Admin'}
              style={styles.buttonSendStyle}
              onPress={this.onPressAdminLogin}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    );
    return uiMain;
  }
}
const mapStateToProps = (state) => {
  return {
    userDetails: state.UserLoginReducer.userDetails,
    serviceState: state.UserLoginReducer.serviceState,
    loaderVisibility: state.UserLoginReducer.loaderVisibility,
    errorMessage: state.UserLoginReducer.errorMessage,
    successMessage: state.UserLoginReducer.successMessage,
    formErrorMessage: state.UserLoginReducer.formErrorMessage,
  };
};

const LoginPage = connect(mapStateToProps)(Login_Page);

export default LoginPage;

import {ErrorEventLogger} from './../../BAL/EventLogger';
import {CustomAsyncStorage} from './../../BAL/LocalDB';
import {USER_PROFILE_DETAILS} from './../../BAL/LocalDB/LocalDBConstants';
import {
  USER_LOGIN_SUCCESS_MESSAGE,
  USER_LOGIN_FAILURE_MESSAGE,
} from './../../Constants/TextConstants';

export const RESET_LOGIN_FORM = 'RESET_LOGIN_FORM';
export const CHECK_REPORT_LOGIN_FORM_ERROR = 'CHECK_REPORT_LOGIN_FORM_ERROR';
export const UPDATE_USER_LOGIN_CREDENTIALS = 'UPDATE_USER_LOGIN_CREDENTIALS';
export const USER_LOGIN_ACCESS = 'USER_LOGIN_ACCESS';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';

export const fetchResetUserLoginDetails = () => ({
  type: RESET_LOGIN_FORM,
});
export const fetchCheckReportLoginFormError = (formErrorMessage) => ({
  type: CHECK_REPORT_LOGIN_FORM_ERROR,
  payload: formErrorMessage,
});
export const fetchUpadteLoginUserDetails = (userDetails) => ({
  type: UPDATE_USER_LOGIN_CREDENTIALS,
  payload: userDetails,
});
export const getuserLoginAccess = () => ({
  type: USER_LOGIN_ACCESS,
});
export const getUserLoginSuccess = (successData) => ({
  type: USER_LOGIN_SUCCESS,
  payload: USER_LOGIN_SUCCESS_MESSAGE,
});
export const getUserLoginFailure = (failureReport) => ({
  type: USER_LOGIN_FAILURE,
  payload: USER_LOGIN_FAILURE_MESSAGE,
});

export function fetchUserLoginAction(userDetails) {
  return async (dispatchUserLogin) => {
    dispatchUserLogin(getuserLoginAccess());
    try {
      CustomAsyncStorage.addDataToStorage(
        USER_PROFILE_DETAILS,
        JSON.stringify(userDetails),
        (successData) => {
          dispatchUserLogin(getUserLoginSuccess(successData));
        },
        (failureData) => {
          dispatchUserLogin(getUserLoginFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchUserLogin(
        getUserLoginFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

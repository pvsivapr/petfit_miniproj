import {
  GET_FEEDBACK,
  GET_FEEDBACK_SUCCESS,
  GET_FEEDBACK_FAILURE,
} from './ViewFeedbackActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';

const initialState = {
  feedbackDetails: [
    // {
    //   feedbackId: '',
    //   feedbackBy: '',
    //   feedbackRating: '',
    //   feedbackType: '',
    //   feedbackNotes: '',
    // },
  ],
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const ViewFeedbackReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FEEDBACK:
      state = {...state, loaderVisibility: true, serviceState: LOADING};
      break;
    case GET_FEEDBACK_SUCCESS:
      state = {
        ...state,
        feedbackDetails: action.payload,
        serviceState: SUCCESS,
        loaderVisibility: false,
      };
      break;
    case GET_FEEDBACK_FAILURE:
      state = {
        ...state,
        formErrorMessage: action.payload,
        serviceState: FAILURE,
        loaderVisibility: false,
      };
      break;
  }
  return state;
};

export default ViewFeedbackReducer;

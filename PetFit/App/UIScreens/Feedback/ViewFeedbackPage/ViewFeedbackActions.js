import {ErrorEventLogger} from '../../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {FEEDBACK_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';

export const GET_FEEDBACK = 'GET_FEEDBACK';
export const GET_FEEDBACK_SUCCESS = 'GET_FEEDBACK_SUCCESS';
export const GET_FEEDBACK_FAILURE = 'GET_FEEDBACK_FAILURE';

export const getFeedback = () => ({
  type: GET_FEEDBACK,
});

export const getFeedbackSuccess = (feedbackDetails) => ({
  type: GET_FEEDBACK_SUCCESS,
  payload: feedbackDetails,
});

export const getFeedbackFailure = () => ({
  type: GET_FEEDBACK_FAILURE,
});

export function fetchGetFeedbackAction(petId) {
  return async (dispatchGetFeedbackAction) => {
    dispatchGetFeedbackAction(getFeedback());
    try {
      CustomAsyncStorage.getDataFromStorage(
        FEEDBACK_DETAILS,
        (success_Data) => {
          let available_Feedback_Data = [];
          if (success_Data !== null && success_Data.response_data !== null) {
            available_Feedback_Data = JSON.parse(success_Data.response_data);
            dispatchGetFeedbackAction(
              getFeedbackSuccess(available_Feedback_Data),
            );
          }
        },
        (failureData) => {
          dispatchGetFeedbackAction(getFeedbackFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchGetFeedbackAction(
        getFeedbackFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

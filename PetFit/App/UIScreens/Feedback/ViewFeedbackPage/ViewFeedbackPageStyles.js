import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';

const screenHeight = Dimensions.get('screen').height;
const screenWidth = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  flatListItemImageStyle: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    marginTop: (screenHeight / 100) * 2.5,
  },
  flatListItemTextStyle: {
    color: '#FFFFFF',
    marginBottom: 5,
  },
  flatListItemTextHolderStyle: {
    flex: 1,
    justifyContent: 'center',
    margin: 10,
  },
  flatlistUnderlineStyle: {
    height: 1,
    backgroundColor: '#FF0000',
    flex: 1,
  },
  flatListHolderStyle: {
    backgroundColor: 'rgb(63,85,97)',
    alignContent: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    paddingRight: 10,
  },
  flatListContainerStyle: {
    marginHorizontal: 10,
    marginTop: 10,
    flex: 1,
    // marginBottom: (screenHeight / 100) * 35,
  },

  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgb(63,85,97)',
  },
});

export default styles;

import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './ViewFeedbackPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import {VIEW_FEEDBACK_PAGE_TITLE} from './../../../Constants/TextConstants';
import {MASTER_HEADER_HAM_BURGER} from './../../../Assets/ImageHelper';
import {fetchGetFeedbackAction} from './ViewFeedbackActions';

const ViewFeedback_Page = (props) => {
  const {
    // route,
    dispatch,
    navigation,
    feedbackDetails,
  } = props;

  useEffect(() => {
    // const petId = route.params.petId;
    dispatch(fetchGetFeedbackAction());
  }, []);

  const onNavigationButtonPresshandler = () => {
    navigation.toggleDrawer();
  };

  const flatListItemView = ({item}) => {
    return (
      <View style={styles.flatListHolderStyle}>
        <View style={styles.flatListItemTextHolderStyle}>
          <Text style={{...styles.flatListItemTextStyle}}>
            {`Feedback given by ${item.feedbackBy}:`}
          </Text>
          <Text style={styles.flatListItemTextStyle}>
            {item.feedbackRating}
          </Text>
          <Text style={styles.flatListItemTextStyle}>
            {`${item.feedbackType.toLowerCase()}:${item.feedbackNotes}`}
          </Text>
        </View>
        <View style={styles.flatlistUnderlineStyle} />
      </View>
    );
  };
  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        <CustomHeader
          title={VIEW_FEEDBACK_PAGE_TITLE}
          showNavigationIcon={true}
          isMasterDetailHeader={true}
          navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          onNavigationButtonPress={onNavigationButtonPresshandler}
        />
        {
          <View style={styles.flatListContainerStyle}>
            <FlatList
              data={feedbackDetails}
              renderItem={flatListItemView}
              keyExtractor={(item) => item.id}
            />
          </View>
        }
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    feedbackDetails: state.ViewFeedbackReducer.feedbackDetails,
    serviceState: state.ViewFeedbackReducer.serviceState,
    loaderVisibility: state.ViewFeedbackReducer.loaderVisibility,
    errorMessage: state.ViewFeedbackReducer.errorMessage,
    successMessage: state.ViewFeedbackReducer.successMessage,
    formErrorMessage: state.ViewFeedbackReducer.formErrorMessage,
  };
};

const ViewFeedbackPage = connect(mapStateToProps)(ViewFeedback_Page);

export default ViewFeedbackPage;

import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './AddFeedbackPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomButton from './../../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
import CustomTextInputField from './../../../CommonComponents/CustomTextInputField';
import {FeedbackDetailConstants} from './../../../Constants/PageNames';
import {addFeedbackPageTextConstants} from './../../../Constants/TextConstants';
import {
  MASTER_HEADER_HAM_BURGER,
  PET_FIT_LOGO,
  BLANK_EXPRESSION_ICON,
  VERY_DISSATISFIED_ICON,
  DISSATISFIED_ICON,
  AVERAGE_ICON,
  SATISFIED_ICON,
  VERY_SATISFIED_ICON,
} from './../../../Assets/ImageHelper';
import {
  fetchUpadteAddFeedbackDetails,
  fetchCheckReportAddFeedbackFormError,
  fetchAddFeedbackAction,
  getAddFeedbackReset,
} from './AddFeedbackActions';
import CustomTouch from '../../../CommonComponents/CustomTouch';
import {useEffect} from 'react';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {USER_PROFILE_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';

const AddFeedback_Page = (props) => {
  const {
    dispatch,
    feedbackDetails,
    navigation,
    loaderVisibility,
    formErrorMessage,
  } = props;

  useEffect(() => {
    // dispatch(getAddFeedbackReset());
    CustomAsyncStorage.getDataFromStorage(
      USER_PROFILE_DETAILS,
      (successData) => {
        try {
          const userDetails = JSON.parse(successData.response_data);
          if (
            userDetails !== null &&
            userDetails !== undefined &&
            userDetails !== {} &&
            userDetails.name !== null &&
            userDetails.name !== undefined &&
            userDetails.name !== ''
          ) {
            let feedback_Details = {};
            feedback_Details = {
              ...feedbackDetails,
              feedbackBy: userDetails.name,
            };
            dispatch(fetchUpadteAddFeedbackDetails(feedback_Details));
          }
        } catch (ex) {}
      },
    );
  }, []);

  const ratingListItems = [
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_1,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_1,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_1,
      icon: VERY_DISSATISFIED_ICON,
    },
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_2,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_2,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_2,
      icon: DISSATISFIED_ICON,
    },
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_3,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_3,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_3,
      icon: AVERAGE_ICON,
    },
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_4,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_4,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_4,
      icon: SATISFIED_ICON,
    },
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_5,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_5,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_5,
      icon: VERY_SATISFIED_ICON,
    },
  ];

  const feedbackListItems = [
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_1,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_1,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_1,
    },
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_2,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_2,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_2,
    },
    {
      id: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_3,
      title: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_3,
      value: addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_TYPE_3,
    },
  ];

  const onNavigationButtonPresshandler = () => {
    navigation.toggleDrawer();
  };

  const onPressAddPet = async () => {
    if (feedbackDetails !== null && feedbackDetails !== undefined) {
      if (
        feedbackDetails.feedbackRating === null ||
        feedbackDetails.feedbackRating === undefined ||
        feedbackDetails.feedbackRating === ''
      ) {
        dispatch(
          fetchCheckReportAddFeedbackFormError('Please enter a valid rating'),
        );
      } else if (
        feedbackDetails.feedbackType === null ||
        feedbackDetails.feedbackType === undefined ||
        feedbackDetails.feedbackType === ''
      ) {
        dispatch(
          fetchCheckReportAddFeedbackFormError(
            'Please select a feedback category',
          ),
        );
      } else if (
        feedbackDetails.feedbackNotes === null ||
        feedbackDetails.feedbackNotes === undefined ||
        feedbackDetails.feedbackNotes === ''
      ) {
        dispatch(fetchCheckReportAddFeedbackFormError('Please enter feedback'));
      } else {
        dispatch(fetchAddFeedbackAction(feedbackDetails));
      }
    } else {
      dispatch(
        fetchCheckReportAddFeedbackFormError(
          'Please fill all mandatory fields',
        ),
      );
    }
  };

  const onTextChangedHandler = (text, inputID) => {
    let feedback_Details = {};
    let isupdated = false;
    if (inputID === FeedbackDetailConstants.FEEDBACK_NOTES) {
      feedback_Details = {...feedbackDetails, feedbackNotes: text};
      isupdated = true;
    } else {
      feedback_Details = {...feedbackDetails};
    }
    if (isupdated) {
      dispatch(fetchUpadteAddFeedbackDetails(feedback_Details));
    }
  };

  const onFeedbackCategoryClickHandler = (text) => {
    let feedback_Details = {};
    feedback_Details = {...feedbackDetails, feedbackType: text};
    dispatch(fetchUpadteAddFeedbackDetails(feedback_Details));
  };

  const onRatingClickHandler = (ratingItem) => {
    let feedback_Details = {};
    feedback_Details = {...feedbackDetails, feedbackRating: ratingItem.value};
    dispatch(fetchUpadteAddFeedbackDetails(feedback_Details));
  };

  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        {/* <View style={styles.mainViewStyle}> */}
        <CustomActivityIndicator visibility={loaderVisibility} />

        <CustomHeader
          title={addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_TITLE}
          showNavigationIcon={true}
          isMasterDetailHeader={false}
          navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          onNavigationButtonPress={onNavigationButtonPresshandler}
        />

        <ScrollView>
          <View style={styles.textInputHolderViewStyle}>
            <View style={styles.imageViewHolderStyle}>
              <Image style={styles.imageViewStyle} source={PET_FIT_LOGO} />
            </View>
            <Text style={styles.boldTextStyle}>
              {addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_HEADER_TEXT}
            </Text>
            <Text style={{...styles.boldTextStyle, ...styles.textStyle}}>
              {addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_INFO_TEXT_1}
            </Text>
            <Text style={styles.textStyle}>
              {addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_INFO_TEXT_2}
            </Text>
            <View style={styles.feedbackButtonHolderStyle}>
              {ratingListItems.map((item) => {
                const ratingItem = {
                  ...item,
                };
                const emojiIcon =
                  feedbackDetails.feedbackRating === item.value
                    ? item.icon
                    : BLANK_EXPRESSION_ICON;
                const holderStyle =
                  feedbackDetails.feedbackRating === item.value
                    ? {
                        ...styles.ratingHolderUnselectedStyle,
                        ...styles.ratingHolderSelectedStyle,
                      }
                    : styles.ratingHolderUnselectedStyle;
                return (
                  <CustomTouch childData={item} onPress={onRatingClickHandler}>
                    <View style={holderStyle}>
                      <Image source={emojiIcon} />
                      <Text style={styles.ratingTextStyle}>{item.title}</Text>
                    </View>
                  </CustomTouch>
                );
              })}
            </View>
            <Text style={styles.textStyle}>
              {addFeedbackPageTextConstants.ADD_FEEDBACK_PAGE_CATEGORY_HEADER}
            </Text>
            <View style={styles.feedbackButtonHolderStyle}>
              {feedbackListItems.map((item) => {
                const ratingItem = {
                  ...item,
                };
                const buttonStyle =
                  feedbackDetails.feedbackType === item.value
                    ? styles.feedbackButtonsSelectedStyle
                    : styles.feedbackButtonsUnSelectedStyle;
                return (
                  <CustomButton
                    childData={item.value}
                    style={buttonStyle}
                    onPress={() => onFeedbackCategoryClickHandler(item.value)}
                    title={item.value}
                  />
                );
              })}
            </View>
            {
              <>
                <Text style={{...styles.boldTextStyle, ...styles.textStyle}}>
                  {addFeedbackPageTextConstants.ADD_FEEDBACK_NOTES_LEGEND_TEXT}
                </Text>
                <CustomTextInputField
                  multiline={true}
                  fontStyle={styles.editorViewStyle}
                  onChangeText={onTextChangedHandler}
                  inputID={FeedbackDetailConstants.FEEDBACK_NOTES}
                  legendTitle={
                    addFeedbackPageTextConstants.ADD_FEEDBACK_NOTES_LEGEND_TEXT
                  }
                  placeHolder={
                    addFeedbackPageTextConstants.ADD_FEEDBACK_NOTES_PLACEHOLDER_TEXT
                  }
                  hintText={
                    addFeedbackPageTextConstants.ADD_FEEDBACK_NOTES_ERROR_HINT_TEXT
                  }
                  value={feedbackDetails.feedbackNotes}
                />
              </>
            }
            {formErrorMessage !== null &&
            formErrorMessage !== undefined &&
            formErrorMessage !== '' ? (
              <Text style={styles.textErrorInfoStyle}>{formErrorMessage}</Text>
            ) : (
              <></>
            )}
            <View style={styles.buttonHolderStyle}>
              <CustomButton
                title={addFeedbackPageTextConstants.ADD_FEEDBACK_BUTTON_TEXT}
                style={styles.buttonSendStyle}
                fontStyle={styles.buttonSendFontStyle}
                onPress={onPressAddPet}
              />
            </View>
          </View>
        </ScrollView>
        {/* </View> */}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    feedbackDetails: state.AddFeedbackReducer.feedbackDetails,
    serviceState: state.AddFeedbackReducer.serviceState,
    loaderVisibility: state.AddFeedbackReducer.loaderVisibility,
    errorMessage: state.AddFeedbackReducer.errorMessage,
    successMessage: state.AddFeedbackReducer.successMessage,
    formErrorMessage: state.AddFeedbackReducer.formErrorMessage,
  };
};

const AddFeedbackPage = connect(mapStateToProps)(AddFeedback_Page);

export default AddFeedbackPage;

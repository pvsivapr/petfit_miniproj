import {ConsoleLogger, ErrorEventLogger} from '../../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {FEEDBACK_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';
import {getDyanmicIDFromDate} from './../../../CommonComponents/GetDynamicID';

export const CHECK_REPORT_ADD_FEEDBACK_FORM_ERROR =
  'CHECK_REPORT_ADD_FEEDBACK_FORM_ERROR';
export const UPDATE_ADD_FEEDBACK_DETAILS = 'UPDATE_ADD_FEEDBACK_DETAILS';
export const ADD_FEEDBACK_ACCESS = 'ADD_FEEDBACK_ACCESS';
export const ADD_FEEDBACK_SUCCESS_MESSAGE = 'ADD_FEEDBACK_SUCCESS_MESSAGE';
export const ADD_FEEDBACK_FAILURE_MESSAGE = 'ADD_FEEDBACK_FAILURE_MESSAGE';
export const ADD_FEEDBACK_RESET = 'ADD_FEEDBACK_RESET';

export const fetchCheckReportAddFeedbackFormError = (formErrorMessage) => ({
  type: CHECK_REPORT_ADD_FEEDBACK_FORM_ERROR,
  payload: formErrorMessage,
});
export const fetchUpadteAddFeedbackDetails = (feedbackDetails) => ({
  type: UPDATE_ADD_FEEDBACK_DETAILS,
  payload: feedbackDetails,
});

export const getAddFeedbackReset = () => ({
  type: ADD_FEEDBACK_RESET,
});

export const getAddFeedbackAccess = () => ({
  type: ADD_FEEDBACK_ACCESS,
});
export const getAddFeedbackSuccess = (successData) => ({
  type: ADD_FEEDBACK_SUCCESS_MESSAGE,
  payload: 'ADD_FEEDBACK_SUCCESS_MESSAGE',
});
export const getAddFeedbackFailure = (failureReport) => ({
  type: ADD_FEEDBACK_FAILURE_MESSAGE,
  payload: 'ADD_FEEDBACK_FAILURE_MESSAGE',
});

export function fetchAddFeedbackAction(feedbackDetails) {
  return async (dispatchAddFeedback) => {
    dispatchAddFeedback(getAddFeedbackAccess()); //FEEDBACK_DETAILS
    try {
      CustomAsyncStorage.getDataFromStorage(
        FEEDBACK_DETAILS,
        (success_Data) => {
          ConsoleLogger(
            'AddPetDetails >>>> CustomAsyncStorage.getDataFromStorage >>>> success_Data',
            success_Data,
          );
          let available_Feedback_Data = [];
          if (success_Data !== null && success_Data.response_data !== null) {
            available_Feedback_Data = JSON.parse(success_Data.response_data);
          }
          let availableFeedbackData = available_Feedback_Data;

          feedbackDetails = {
            ...feedbackDetails,
            feedbackId: getDyanmicIDFromDate(),
          };
          availableFeedbackData.push(feedbackDetails);

          CustomAsyncStorage.addDataToStorage(
            FEEDBACK_DETAILS,
            JSON.stringify(availableFeedbackData),
            (successData) => {
              dispatchAddFeedback(
                getAddFeedbackSuccess('Data added successfully'),
              );
              dispatchAddFeedback(getAddFeedbackReset());
            },
            (failureData) => {
              dispatchAddFeedback(getAddFeedbackFailure(failureData));
            },
          );
        },
        (failureData) => {
          dispatchAddFeedback(getAddFeedbackFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchAddFeedback(
        getAddFeedbackFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

import {
  CHECK_REPORT_ADD_FEEDBACK_FORM_ERROR,
  UPDATE_ADD_FEEDBACK_DETAILS,
  ADD_FEEDBACK_ACCESS,
  ADD_FEEDBACK_SUCCESS_MESSAGE,
  ADD_FEEDBACK_FAILURE_MESSAGE,
  ADD_FEEDBACK_RESET,
} from './AddFeedbackActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';

const initialState = {
  feedbackDetails: {
    feedbackId: '',
    feedbackBy: '',
    feedbackRating: '',
    feedbackType: '',
    feedbackNotes: '',
  },
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const AddFeedbackReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECK_REPORT_ADD_FEEDBACK_FORM_ERROR:
      state = {
        ...state,
        formErrorMessage: action.payload,
        serviceState: INACTIVE,
        loaderVisibility: false,
      };
      break;
    case UPDATE_ADD_FEEDBACK_DETAILS:
      state = {
        ...state,
        feedbackDetails: action.payload,
        formErrorMessage: '',
        serviceState: INACTIVE,
        loaderVisibility: false,
      };
      break;
    case ADD_FEEDBACK_ACCESS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case ADD_FEEDBACK_SUCCESS_MESSAGE:
      state = {
        ...state,
        serviceState: SUCCESS,
        successMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_FEEDBACK_FAILURE_MESSAGE:
      state = {
        ...state,
        serviceState: FAILURE,
        errorMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_FEEDBACK_RESET:
      state = {
        feedbackDetails: {
          feedbackId: '',
          feedbackBy: '',
          feedbackRating: '',
          feedbackType: '',
          feedbackNotes: '',
        },
        serviceState: INACTIVE,
        loaderVisibility: false,
        successMessage: '',
        errorMessage: '',
        formErrorMessage: '',
      };
      break;
  }
  return state;
};

export default AddFeedbackReducer;

import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import AppColors from './../../../Constants/Colors';
import {
  APP_MIN_INPUT_HEIGHT,
  APP_MIN_INPUT_WIDTH,
} from './../../../Constants/PageDimensions';
import {
  TEXT_LINK_COLOR,
  TEXT_INFO_COLOR,
} from './../../../Constants/AppStyleConstants';

const screenHeight = Dimensions.get('screen').height;
const screenWidth = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  textErrorInfoStyle: {
    color: 'maroon',
  },

  boldTextStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 8,
    marginBottom: 5,
  },
  textStyle: {
    fontSize: 15,
    marginTop: 8,
    marginBottom: 5,
  },

  ratingTextStyle: {
    fontSize: 8,
  },
  ratingHolderUnselectedStyle: {
    flex: 1,
    width: (screenWidth / 100) * 20 - 10,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  },
  ratingHolderSelectedStyle: {
    backgroundColor: 'rgb(102,102,255)',
  },

  feedbackButtonsSelectedStyle: {
    flex: 1,
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
    backgroundColor: 'rgb(102,102,255)',
    borderRadius: 10,
    width: (screenWidth / 100) * 33 - 15,
    height: (screenHeight / 100) * 8 - 15,
    // height: 50,
  },
  feedbackButtonsUnSelectedStyle: {
    flex: 1,
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
    backgroundColor: '#000000',
    borderRadius: 10,
    width: (screenWidth / 100) * 33 - 15,
    height: (screenHeight / 100) * 8 - 15,
    // height: 50,
  },
  feedbackButtonHolderStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },

  editorViewStyle: {
    minHeight: 150,
    height: 150,
    textAlignVertical: 'top',
  },

  imageViewStyle: {
    height: 150,
    width: 150,
  },
  imageViewHolderStyle: {
    height: 150,
    width: 150,
    margin: 10,
    marginBottom: 10,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: 'green',
    alignSelf: 'center',
  },

  buttonSendStyle: {
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
    backgroundColor: '#000000',
    borderRadius: 10,
    width: (screenWidth / 100) * 25 - 20,
    height: 50,
  },
  buttonSendFontStyle: {
    color: '#FFFFFF',
  },
  buttonHolderStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },

  textInputHolderViewStyle: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingBottom: 10,
    margin: 10,
    backgroundColor: 'rgb(170,170,170)',
  },

  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgb(225,227,230)',
  },
});

export default styles;

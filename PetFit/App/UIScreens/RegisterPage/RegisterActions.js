import {ErrorEventLogger} from './../../BAL/EventLogger';
import {CustomAsyncStorage, ExecuteDBOperations} from './../../BAL/LocalDB';
import {USER_PROFILE_DETAILS} from './../../BAL/LocalDB/LocalDBConstants';
import {
  USER_REGISTER_SUCCESS_MESSAGE,
  USER_REGISTER_FAILURE_MESSAGE,
} from './../../Constants/TextConstants';

export const CHECK_REPORT_REGISTER_FORM_ERROR =
  'CHECK_REPORT_REGISTER_FORM_ERROR';
export const UPDATE_REGISTER_USER_CREDENTIALS = 'UPDATE_USER_CREDENTIALS';
export const USER_REGISTER_ACCESS = 'USER_REGISTER_ACCESS';
export const USER_REGISTER_SUCCESS = 'USER_REGISTER_SUCCESS';
export const USER_REGISTER_FAILURE = 'USER_REGISTER_FAILURE';

export const fetchCheckReportRegisterFormError = (formErrorMessage) => ({
  type: CHECK_REPORT_REGISTER_FORM_ERROR,
  payload: formErrorMessage,
});
export const fetchUpadteRegisterUserDetails = (userDetails) => ({
  type: UPDATE_REGISTER_USER_CREDENTIALS,
  payload: userDetails,
});
export const getuserRegisterAccess = () => ({
  type: USER_REGISTER_ACCESS,
});
export const getUserRegisterSuccess = (successData) => ({
  type: USER_REGISTER_SUCCESS,
  payload: USER_REGISTER_SUCCESS_MESSAGE,
});
export const getUserRegisterFailure = (failureReport) => ({
  type: USER_REGISTER_FAILURE,
  payload: USER_REGISTER_FAILURE_MESSAGE,
});

export function fetchUserRegisterAction(userDetails) {
  return async (dispatchUserRegister) => {
    dispatchUserRegister(getuserRegisterAccess());
    try {
      // ExecuteDBOperations.addDataToStorage(USER_PROFILE_DETAILS, JSON.stringify(userDetails), (successData) => {
      //   dispatchUserRegister(getUserRegisterSuccess(successData));
      // }, (failureData) => {
      //   dispatchUserRegister(getUserRegisterFailure(failureData));
      // });
    } catch (error) {
      ErrorEventLogger(error);
      dispatchUserRegister(
        getUserRegisterFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

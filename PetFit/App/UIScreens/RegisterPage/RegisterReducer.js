import {
  USER_REGISTER_SUCCESS_MESSAGE,
  UPDATE_REGISTER_USER_CREDENTIALS,
  USER_REGISTER_ACCESS,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_FAILURE,
} from './RegisterActions';
import {FAILURE, INACTIVE, LOADING, SUCCESS} from './../../Constants/PageNames';
import {ConsoleLogger} from './../../BAL/EventLogger';

const initialState = {
  userDetails: {
    name: '',
    password: '',
  },
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const UserRegisterReducer = (state = initialState, action) => {
  state = {
    ...state,
    serviceState: INACTIVE,
    loaderVisibility: false,
    successMessage: '',
    errorMessage: '',
    formErrorMessage: '',
  };
  switch (action.type) {
    case USER_REGISTER_SUCCESS_MESSAGE:
      state = {...state, formErrorMessage: action.payload};
      break;
    case UPDATE_REGISTER_USER_CREDENTIALS:
      ConsoleLogger(
        'RegisterReducer >>>> UserRegisterReducer >>>> UPDATE_USER_CREDENTIALS >>>> action.payload: ',
        action.payload,
      );
      state = {...state, userDetails: action.payload};
      break;
    case USER_REGISTER_ACCESS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case USER_REGISTER_SUCCESS:
      state = {
        ...state,
        serviceState: SUCCESS,
        successMessage: action.payload,
      };
      break;
    case USER_REGISTER_FAILURE:
      state = {
        ...state,
        serviceState: FAILURE,
        errorMessage: action.payload,
      };
      break;
    // case USER_REGISTER_RESET:
    //   state = {...state};
    //   break;
  }
  return state;
};

export default UserRegisterReducer;

import React from 'react';
import {View, Text, Platform, KeyboardAvoidingView} from 'react-native';
import {connect} from 'react-redux';
import styles from './RegisterPageStyles';
import CustomButton from './../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../CommonComponents/CustomActivityIndicator';
import CustomTextInputField from './../../CommonComponents/CustomTextInputField';
import {ConsoleLogger} from './../../BAL/EventLogger';
import {
  LOGIN_PAGE,
  DRAWER_NAVIGATOR,
  SUCCESS,
} from './../../Constants/PageNames';
import {UserCredentialConstants} from './../../Constants/PageNames';
import {UserCredentialTextConstants} from './../../Constants/TextConstants';

import {
  fetchCheckReportRegisterFormError,
  fetchUpadteRegisterUserDetails,
  fetchUserRegisterAction,
} from './RegisterActions';

class Register_Page extends React.Component {
  constuctor() {
    // super(props);
  }
/*
    "@react-native-community/async-storage": "~1.12.0",
    "@react-native-community/cameraroll": "^4.0.0",
    "@react-native-community/datetimepicker": "3.0.4",
    "@react-native-community/masked-view": "0.1.10",
    "@react-native-community/picker": "^1.8.1",
    "@react-navigation/bottom-tabs": "^5.6.1",
    "@react-navigation/drawer": "^5.8.4",
    "@react-navigation/material-top-tabs": "^5.3.13",
    "@react-navigation/native": "^5.6.1",
    "@react-navigation/stack": "^5.6.2",
    "react": "16.13.1",
    "react-native": "0.63.4",
    "react-native-carousel-view": "^0.5.1",
    "react-native-gesture-handler": "~1.8.0",
    "react-native-image-picker": "^3.1.4",
    "react-native-paper": "3.6.0",
    "react-native-reanimated": "~1.13.0",
    "react-native-safe-area-context": "3.1.9",
    "react-native-safe-area-view": "^1.1.1",
    "react-native-screens": "~2.15.0",
    "react-native-snap-carousel": "^3.9.1",
    "react-native-svg": "12.1.0",
    "react-native-tab-view": "^2.15.2",
    "react-redux": "^7.2.0",
    "redux": "^4.0.5",
    "redux-thunk": "^2.3.0",
    "uuid": "^8.3.2"
*/
  componentWillReceiveProps(nextProps) {
    ConsoleLogger(
      'LoginPage >>>> componentWillReceiveProps >>>> nextProps: ',
      nextProps,
    );
    if (nextProps.serviceState === SUCCESS) {
      const {navigation} = this.props;
      ConsoleLogger(
        'LoginPage >>>> componentWillReceiveProps >>>> navigation: ',
        navigation,
      );
      if (navigation !== null && navigation !== undefined) {
        navigation.navigate(DRAWER_NAVIGATOR);
      }
    }
  }

  onPressRegister = async () => {
    ConsoleLogger('LoginPage >>>> onPressLogin >>>> this.props: ', this.props);
    const {userDetails, dispatch} = this.props;
    if (userDetails !== null && userDetails !== undefined) {
      if (
        userDetails.name === null ||
        userDetails.name === undefined ||
        userDetails.name === ''
      ) {
        dispatch(
          fetchCheckReportRegisterFormError('Please enter a valid user name'),
        );
      } else if (
        userDetails.password === null ||
        userDetails.password === undefined ||
        userDetails.password === ''
      ) {
        dispatch(
          fetchCheckReportRegisterFormError('Please enter a valid password'),
        );
      } else {
        dispatch(fetchUserRegisterAction(userDetails));
      }
    } else {
      dispatch(
        fetchCheckReportRegisterFormError('Please fill all mandatory fields'),
      );
    }
  };
  onTextChangedHandler = (text, inputID) => {
    const {userDetails, dispatch} = this.props;
    let user_Details = {};
    let isupdated = false;
    if (inputID === UserCredentialConstants.USER_NAME) {
      user_Details = {...userDetails, name: text};
      isupdated = true;
    } else if (inputID === UserCredentialConstants.USER_PASSWORD) {
      user_Details = {...userDetails, password: text};
      isupdated = true;
    } else {
    }
    if (isupdated) {
      dispatch(fetchUpadteRegisterUserDetails(user_Details));
    }
  };

  render() {
    const {loaderVisibility, formErrorMessage} = this.props;
    const uiMain = (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.mainViewStyle}>
        <View style={styles.mainViewStyle}>
          <CustomActivityIndicator visibility={loaderVisibility} />

          <View style={styles.textInputHolderViewStyle}>
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_FIRST_NAME}
              placeHolder={
                UserCredentialTextConstants.USER_FNAME_PLACEHOLDER_TEXT
              }
              legendTitle={UserCredentialTextConstants.USER_FNAME_LEGEND_TEXT}
              hintText={UserCredentialTextConstants.USER_FNAME_ERROR_HINT_TEXT}
              secureTextEntry={true}
            />
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_LAST_NAME}
              placeHolder={
                UserCredentialTextConstants.USER_LNAME_PLACEHOLDER_TEXT
              }
              legendTitle={UserCredentialTextConstants.USER_LNAME_LEGEND_TEXT}
              hintText={UserCredentialTextConstants.USER_LNAME_ERROR_HINT_TEXT}
            />
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_NAME}
              placeHolder={
                UserCredentialTextConstants.USER_NAME_PLACEHOLDER_TEXT
              }
              legendTitle={UserCredentialTextConstants.USER_NAME_LEGEND_TEXT}
              hintText={UserCredentialTextConstants.USER_NAME_ERROR_HINT_TEXT}
            />
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_PASSWORD}
              placeHolder={
                UserCredentialTextConstants.USER_PASSWORD_PLACEHOLDER_TEXT
              }
              legendTitle={
                UserCredentialTextConstants.USER_PASSWORD_LEGEND_TEXT
              }
              hintText={
                UserCredentialTextConstants.USER_PASSWORD_ERROR_HINT_TEXT
              }
              secureTextEntry={true}
            />
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_EMAIL_ID}
              placeHolder={
                UserCredentialTextConstants.USER_EMAIL_PLACEHOLDER_TEXT
              }
              legendTitle={UserCredentialTextConstants.USER_EMAIL_LEGEND_TEXT}
              hintText={UserCredentialTextConstants.USER_EMAIL_ERROR_HINT_TEXT}
            />
            <CustomTextInputField
              onChangeText={this.onTextChangedHandler}
              inputID={UserCredentialConstants.USER_MOBILE_NUMBER}
              placeHolder={
                UserCredentialTextConstants.USER_MOBILE_PLACEHOLDER_TEXT
              }
              legendTitle={UserCredentialTextConstants.USER_MOBILE_LEGEND_TEXT}
              hintText={UserCredentialTextConstants.USER_MOBILE_ERROR_HINT_TEXT}
            />
            {formErrorMessage !== null &&
            formErrorMessage !== undefined &&
            formErrorMessage !== '' ? (
              <Text style={styles.textErrorInfoStyle}>{formErrorMessage}</Text>
            ) : (
              <></>
            )}
            <CustomButton
              title={'Register'}
              style={styles.buttonSendStyle}
              onPress={this.onPressRegister}
            />
            <Text style={styles.textStyle}>
              {'If you have an account, then please '}
              <Text
                style={styles.textLinkStyle}
                onPress={() => {
                  this.props.navigation.navigate(LOGIN_PAGE);
                }}>
                Login
              </Text>
            </Text>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
    return uiMain;
  }
}
const mapStateToProps = (state) => {
  return {
    userDetails: state.UserRegisterReducer.userDetails,
    serviceState: state.UserRegisterReducer.serviceState,
    loaderVisibility: state.UserRegisterReducer.loaderVisibility,
    errorMessage: state.UserRegisterReducer.errorMessage,
    successMessage: state.UserRegisterReducer.successMessage,
    formErrorMessage: state.UserRegisterReducer.formErrorMessage,
  };
};

const RegisterPage = connect(mapStateToProps)(Register_Page);

export default RegisterPage;

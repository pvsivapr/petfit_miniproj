import React from 'react';
import {StyleSheet} from 'react-native';
import AppColors from './../../Constants/Colors';
import {
  APP_MIN_INPUT_HEIGHT,
  APP_MIN_INPUT_WIDTH,
} from './../../Constants/PageDimensions';
import {
  TEXT_LINK_COLOR,
  TEXT_INFO_COLOR,
} from './../../Constants/AppStyleConstants';

const styles = StyleSheet.create({
  textStyle: {
    color: TEXT_INFO_COLOR,
  },
  textLinkStyle: {
    color: 'blue',
  },
  textErrorInfoStyle: {
    color: 'maroon',
  },
  textInputHolderViewStyle: {
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  buttonSendStyle: {
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
  },
  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default styles;

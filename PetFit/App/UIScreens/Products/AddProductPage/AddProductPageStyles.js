import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import AppColors from './../../../Constants/Colors';
import {
  APP_MIN_INPUT_HEIGHT,
  APP_MIN_INPUT_WIDTH,
} from './../../../Constants/PageDimensions';
import {
  TEXT_LINK_COLOR,
  TEXT_INFO_COLOR,
} from './../../../Constants/AppStyleConstants';

const screenHeight = Dimensions.get('screen').height;
const screenWidth = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  textErrorInfoStyle: {
    color: 'maroon',
  },

  imageViewStyle: {
    height: 150,
    width: 150,
  },
  imageViewHolderStyle: {
    height: 150,
    width: 150,
    margin: 10,
    marginBottom: 10,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: 'green',
    alignSelf: 'center',
  },

  buttonSendStyle: {
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
    backgroundColor: '#FFFFFF',
    borderRadius: 0,
    // marginHorizontal: 5,
    // width: (screenWidth / 100) * 50 - 20,
  },
  buttonSendFontStyle: {
    color: '#000000',
  },
  buttonHolderStyle: {
    // flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },

  textInputHolderViewStyle: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingBottom: 10,
    margin: 10,
    backgroundColor: 'rgb(170,170,170)',
  },

  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgb(225,227,230)',
  },
});

export default styles;

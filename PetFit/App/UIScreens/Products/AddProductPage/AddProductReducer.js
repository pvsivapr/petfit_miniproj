import {
  CHECK_REPORT_ADD_PRODUCT_FORM_ERROR,
  UPDATE_ADD_PRODUCT_DETAILS,
  ADD_PRODUCT_ACCESS,
  ADD_PRODUCT_SUCCESS,
  ADD_PRODUCT_FAILURE,
  ADD_PRODUCT_RESET,
} from './AddProductActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';

const initialState = {
  productDetails: {
    productId: '',
    productName: '',
    productCost: '',
    productImageURL: '',
    availableProductQuantity: '',
  },
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const AddProductReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECK_REPORT_ADD_PRODUCT_FORM_ERROR:
      state = {...state, formErrorMessage: action.payload};
      break;
    case UPDATE_ADD_PRODUCT_DETAILS:
      state = {...state, productDetails: action.payload};
      break;
    case ADD_PRODUCT_ACCESS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case ADD_PRODUCT_SUCCESS:
      state = {
        ...state,
        serviceState: SUCCESS,
        productDetails: action.payload,
        successMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_PRODUCT_FAILURE:
      state = {
        ...state,
        serviceState: FAILURE,
        errorMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_PRODUCT_RESET:
      state = {
        ...state,
        productDetails: {},
        serviceState: INACTIVE,
        loaderVisibility: false,
      };
      break;
  }
  return state;
};

export default AddProductReducer;

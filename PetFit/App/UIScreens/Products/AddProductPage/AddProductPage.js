import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './AddProductPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomButton from './../../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
import CustomTextInputField from './../../../CommonComponents/CustomTextInputField';
import {ConsoleLogger} from './../../../BAL/EventLogger';
import {ProductDetailConstants} from './../../../Constants/PageNames';
import {
  ProductDetailTextConstants,
  ADD_PRODUCT_PAGE_TITLE,
  ADD_PRODUCT_ADD_BUTTON_TEXT,
} from './../../../Constants/TextConstants';
import {
  MASTER_HEADER_HAM_BURGER,
  PET_PIC_PLACEHOLDER,
  CAMERA_ICON,
} from './../../../Assets/ImageHelper';
import {PET_FIT_LOGO} from './../../../Assets/ImageHelper';
import {
  fetchCheckReportAddProductFormError,
  fetchUpadteAddProductDetails,
  fetchAddProductAction,
  fetchAddProductReset,
} from './AddProductActions';

const AddProduct_Page = (props) => {
  const {dispatch, productDetails, navigation} = props;

  useEffect(() => {
    dispatch(fetchAddProductReset());
  },[]);

  const onNavigationButtonPresshandler = () => {
    navigation.toggleDrawer();
  };
  const onPressAddPet = async () => {
    if (productDetails !== null && productDetails !== undefined) {
      if (
        productDetails.productName === null ||
        productDetails.productName === undefined ||
        productDetails.productName === ''
      ) {
        dispatch(
          fetchCheckReportAddProductFormError(
            'Please enter a valid product name',
          ),
        );
      } else if (
        productDetails.productCost === null ||
        productDetails.productCost === undefined ||
        productDetails.productCost === ''
      ) {
        dispatch(
          fetchCheckReportAddProductFormError(
            'Please enter a valid product price',
          ),
        );
        // } else if (
        //   productDetails.productImageURL === null ||
        //   productDetails.productImageURL === undefined ||
        //   productDetails.productImageURL === ''
        // ) {
        //   dispatch(
        //     fetchCheckReportAddProductFormError('Please enter a valid user name'),
        //   );
      } else if (
        productDetails.availableProductQuantity === null ||
        productDetails.availableProductQuantity === undefined ||
        productDetails.availableProductQuantity === ''
      ) {
        dispatch(
          fetchCheckReportAddProductFormError(
            'Please enter the stock available',
          ),
        );
      } else {
        dispatch(fetchAddProductAction(productDetails));
      }
    } else {
      dispatch(
        fetchCheckReportAddProductFormError('Please fill all mandatory fields'),
      );
    }
  };

  const onTextChangedHandler = (text, inputID) => {
    let product_Details = productDetails;
    let isupdated = false;
    if (inputID === ProductDetailConstants.PRODUCT_NAME) {
      product_Details = {...productDetails, productName: text};
      isupdated = true;
    } else if (inputID === ProductDetailConstants.PRODUCT_COST) {
      product_Details = {...productDetails, productCost: text};
      isupdated = true;
    } else if (inputID === ProductDetailConstants.PRODUCT_IMAGE_URL) {
      product_Details = {...productDetails, productImageURL: text};
      isupdated = true;
    } else if (inputID === ProductDetailConstants.PRODUCT_AVAILABLE_STOCK) {
      product_Details = {...productDetails, availableProductQuantity: text};
      isupdated = true;
    } else {
      product_Details = {...productDetails};
    }
    if (isupdated) {
      dispatch(fetchUpadteAddProductDetails(product_Details));
    }
  };

  const onCameraVideoIconClickHandler = () => {};

  const {loaderVisibility, formErrorMessage} = props;
  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        {/* <View style={styles.mainViewStyle}> */}
        <CustomActivityIndicator visibility={loaderVisibility} />

        <CustomHeader
          title={ADD_PRODUCT_PAGE_TITLE}
          showNavigationIcon={true}
          isMasterDetailHeader={false}
          navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          onNavigationButtonPress={onNavigationButtonPresshandler}
        />

        <ScrollView>
          <View style={styles.textInputHolderViewStyle}>
            <View style={styles.imageViewHolderStyle}>
              <Image style={styles.imageViewStyle} source={PET_FIT_LOGO} />
            </View>
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={ProductDetailConstants.PRODUCT_NAME}
              isFullEntry={false}
              legendTitle={ProductDetailTextConstants.PRODUCT_NAME_LEGEND_TEXT}
              placeHolder={
                ProductDetailTextConstants.PRODUCT_NAME_PLACEHOLDER_TEXT
              }
              hintText={ProductDetailTextConstants.PRODUCT_NAME_ERROR_HINT_TEXT}
              value={productDetails.productName}
            />
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={ProductDetailConstants.PRODUCT_COST}
              isFullEntry={false}
              legendTitle={ProductDetailTextConstants.PRODUCT_COST_LEGEND_TEXT}
              placeHolder={
                ProductDetailTextConstants.PRODUCT_COST_PLACEHOLDER_TEXT
              }
              hintText={ProductDetailTextConstants.PRODUCT_COST_ERROR_HINT_TEXT}
              value={productDetails.productCost}
            />
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={ProductDetailConstants.PRODUCT_IMAGE_URL}
              isFullEntry={false}
              legendTitle={ProductDetailTextConstants.PRODUCT_IMAGE_LEGEND_TEXT}
              placeHolder={
                ProductDetailTextConstants.PRODUCT_IMAGE_PLACEHOLDER_TEXT
              }
              hintText={
                ProductDetailTextConstants.PRODUCT_IMAGE_ERROR_HINT_TEXT
              }
              value={productDetails.productImageURL}
            />
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={ProductDetailConstants.PRODUCT_AVAILABLE_STOCK}
              isFullEntry={false}
              legendTitle={
                ProductDetailTextConstants.PRODUCT_AVAILABLE_STOCK_LEGEND_TEXT
              }
              placeHolder={
                ProductDetailTextConstants.PRODUCT_AVAILABLE_STOCK_PLACEHOLDER_TEXT
              }
              hintText={
                ProductDetailTextConstants.PRODUCT_AVAILABLE_STOCK_ERROR_HINT_TEXT
              }
              value={productDetails.availableProductQuantity}
            />
            {formErrorMessage !== null &&
            formErrorMessage !== undefined &&
            formErrorMessage !== '' ? (
              <Text style={styles.textErrorInfoStyle}>{formErrorMessage}</Text>
            ) : (
              <></>
            )}
            <View style={styles.buttonHolderStyle}>
              <CustomButton
                title={ADD_PRODUCT_ADD_BUTTON_TEXT}
                style={styles.buttonSendStyle}
                fontStyle={styles.buttonSendFontStyle}
                onPress={onPressAddPet}
              />
            </View>
          </View>
        </ScrollView>
        {/* </View> */}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    productDetails: state.AddProductReducer.productDetails,
    serviceState: state.AddProductReducer.serviceState,
    loaderVisibility: state.AddProductReducer.loaderVisibility,
    errorMessage: state.AddProductReducer.errorMessage,
    successMessage: state.AddProductReducer.successMessage,
    formErrorMessage: state.AddProductReducer.formErrorMessage,
  };
};

const AddProductPage = connect(mapStateToProps)(AddProduct_Page);

export default AddProductPage;

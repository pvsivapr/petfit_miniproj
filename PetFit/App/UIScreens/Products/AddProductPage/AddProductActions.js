import {ConsoleLogger, ErrorEventLogger} from '../../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {PRODUCT_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';
import {getDyanmicIDFromDate} from '../../../CommonComponents/GetDynamicID';
import {fetchGetProductsAction} from '../ViewProductsPage/ViewProductsActions';

export const CHECK_REPORT_ADD_PRODUCT_FORM_ERROR =
  'CHECK_REPORT_ADD_PRODUCT_FORM_ERROR';
export const UPDATE_ADD_PRODUCT_DETAILS = 'UPDATE_ADD_PRODUCT_DETAILS';
export const ADD_PRODUCT_ACCESS = 'ADD_PRODUCT_ACCESS';
export const ADD_PRODUCT_SUCCESS = 'ADD_PRODUCT_SUCCESS';
export const ADD_PRODUCT_FAILURE = 'ADD_PRODUCT_FAILURE';
export const ADD_PRODUCT_RESET = 'ADD_PRODUCT_RESET';

export const fetchCheckReportAddProductFormError = (formErrorMessage) => ({
  type: CHECK_REPORT_ADD_PRODUCT_FORM_ERROR,
  payload: formErrorMessage,
});
export const fetchUpadteAddProductDetails = (petDetails) => ({
  type: UPDATE_ADD_PRODUCT_DETAILS,
  payload: petDetails,
});
export const getAddProductAccess = () => ({
  type: ADD_PRODUCT_ACCESS,
});
export const getAddProductSuccess = (successData) => ({
  type: ADD_PRODUCT_SUCCESS,
  payload: successData,
});
export const getAddProductFailure = (failureReport) => ({
  type: ADD_PRODUCT_FAILURE,
  payload: failureReport,
});

export function fetchAddProductAction(productDetails) {
  return async (dispatchAddProduct) => {
    dispatchAddProduct(getAddProductAccess());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PRODUCT_DETAILS,
        (success_Data) => {
          ConsoleLogger(
            'AddProductActions >>>> CustomAsyncStorage.getDataFromStorage >>>> success_Data',
            success_Data,
          );
          let available_Product_Data = [];
          if (success_Data !== null && success_Data.response_data !== null) {
            available_Product_Data = JSON.parse(success_Data.response_data);
          }
          let availableProductData = available_Product_Data;
          productDetails = {
            ...productDetails,
            productId: getDyanmicIDFromDate(),
          };
          availableProductData.push(productDetails);
          // for (var i = 0; i < 5; i++) {
          //   productDetails = {
          //     ...productDetails,
          //     productName: getDyanmicIDFromDate() + '____' + i.toString(),
          //     productId: getDyanmicIDFromDate() + i.toString(),
          //   };
          //   availableProductData.push(productDetails);
          // }
          CustomAsyncStorage.addDataToStorage(
            PRODUCT_DETAILS,
            JSON.stringify(availableProductData),
            (successData) => {
              dispatchAddProduct(fetchGetProductsAction());
              dispatchAddProduct(
                getAddProductSuccess('Data added successfully'),
              );
              dispatchAddProduct(fetchAddProductReset());
            },
            (failureData) => {
              dispatchAddProduct(getAddProductFailure(failureData));
            },
          );
        },
        (failureData) => {
          dispatchAddProduct(getAddProductFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchAddProduct(
        getAddProductFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

export const fetchAddProductReset = () => ({
  type: ADD_PRODUCT_RESET,
});

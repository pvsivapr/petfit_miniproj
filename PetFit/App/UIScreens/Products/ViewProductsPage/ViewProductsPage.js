import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './ViewProductsPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
import {ConsoleLogger} from './../../../BAL/EventLogger';
import {VIEW_PRODUCTS_PAGE_TITLE} from './../../../Constants/TextConstants';
import {
  MASTER_HEADER_HAM_BURGER,
  DELETE_ICON,
} from './../../../Assets/ImageHelper';
import {
  fetchGetProductsAction,
  fetchDeleteProductsAction,
} from './ViewProductsActions';
import CustomTouch from '../../../CommonComponents/CustomTouch';

const ViewProducts_Page = (props) => {
  const {
    dispatch,
    loaderVisibility,
    formErrorMessage,
    navigation,
    productDetails,
  } = props;

  useEffect(() => {
    navigation.closeDrawer();
    dispatch(fetchGetProductsAction());
  }, [dispatch, navigation]);

  const onNavigationButtonPresshandler = () => {
    navigation.toggleDrawer();
  };

  const deleteScheduleClickHandler = (productItem) => {
    ConsoleLogger(
      'ViewProductsPage >>>> deleteScheduleClickHandler >>>> productItem',
      productItem,
    );
    dispatch(fetchDeleteProductsAction(productItem));
  };

  const flatListItemView = ({item}) => {
    return (
      <View style={styles.flatListHolderStyle}>
        <Image
          style={styles.flatListItemImageStyle}
          source={{uri: 'productImageURL'}}
        />
        <View style={styles.flatListItemTextHolderStyle}>
          <Text style={{...styles.flatListItemTextStyle, fontWeight: 'bold'}}>
            {item.productName}
          </Text>
          <Text
            style={
              styles.flatListItemTextStyle
            }>{`₹ ${item.productCost}`}</Text>
          <Text style={styles.flatListItemTextStyle}>
            {item.availableProductQuantity}
          </Text>
        </View>
        <CustomTouch childData={item} onPress={deleteScheduleClickHandler}>
          <Image style={styles.flatListItemImageStyle} source={DELETE_ICON} />
        </CustomTouch>
      </View>
    );
  };
  ConsoleLogger(
    'ViewProductsPage >>>> ViewProducts_Page >>>> Test Logging',
    props,
  );
  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <CustomActivityIndicator visibility={loaderVisibility} />
      <SafeAreaView style={styles.mainViewStyle}>
        <CustomHeader
          title={VIEW_PRODUCTS_PAGE_TITLE}
          showNavigationIcon={true}
          isMasterDetailHeader={true}
          navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          onNavigationButtonPress={onNavigationButtonPresshandler}
        />
        {
          <View style={styles.flatListContainerStyle}>
            <FlatList
              data={productDetails}
              renderItem={flatListItemView}
              keyExtractor={(item) => item.id}
            />
          </View>
        }
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    productDetails: state.ViewProductsReducer.productDetails,
    serviceState: state.ViewProductsReducer.serviceState,
    loaderVisibility: state.ViewProductsReducer.loaderVisibility,
    errorMessage: state.ViewProductsReducer.errorMessage,
    successMessage: state.ViewProductsReducer.successMessage,
    formErrorMessage: state.ViewProductsReducer.formErrorMessage,
  };
};

const ViewProductsPage = connect(mapStateToProps)(ViewProducts_Page);

export default ViewProductsPage;

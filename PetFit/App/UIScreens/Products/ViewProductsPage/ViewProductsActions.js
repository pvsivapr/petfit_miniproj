import {ConsoleLogger, ErrorEventLogger} from '../../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {PRODUCT_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';

export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_PRODUCTS_SUCCESS = 'GET_PRODUCTS_SUCCESS';
export const GET_PRODUCTS_FAILURE = 'GET_PRODUCTS_FAILURE';
export const DELETE_PRODUCTS = 'DELETE_PRODUCTS';
export const DELETE_PRODUCTS_SUCCESS = 'DELETE_PRODUCTS_SUCCESS';
export const DELETE_PRODUCTS_FAILURE = 'DELETE_PRODUCTS_FAILURE';

export const getProducts = () => ({
  type: GET_PRODUCTS,
});

export const getProductsSuccess = (productsData) => ({
  type: GET_PRODUCTS_SUCCESS,
  payload: productsData,
});

export const getProductsFailure = (failureData) => ({
  type: GET_PRODUCTS_FAILURE,
  payload: failureData,
});

export const deleteProducts = () => ({
  type: DELETE_PRODUCTS,
});

export const deleteProductsSuccess = (productsData) => ({
  type: DELETE_PRODUCTS_SUCCESS,
  payload: productsData,
});

export const deleteProductsFailure = (failureData) => ({
  type: DELETE_PRODUCTS_FAILURE,
  payload: failureData,
});

export function fetchGetProductsAction() {
  return async (dispatchViewProducts) => {
    dispatchViewProducts(getProducts());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PRODUCT_DETAILS,
        (success_Data) => {
          const available_Products_Data = JSON.parse(
            success_Data.response_data,
          );
          dispatchViewProducts(getProductsSuccess(available_Products_Data));
        },
        (failureData) => {
          dispatchViewProducts(getProductsFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchViewProducts(
        getProductsFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

export function fetchDeleteProductsAction(productItem) {
  return async (dispatchDeleteProducts) => {
    dispatchDeleteProducts(deleteProducts());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PRODUCT_DETAILS,
        (success_Data) => {
          if (success_Data !== null && success_Data.response_data !== null) {
            let available_Product_Data = JSON.parse(success_Data.response_data);
            // const itemIndex = available_Product_Data.indexOf((item) => item.productId ==);

            const itemIndex = available_Product_Data.findIndex(
              (item) => item.productId === productItem.productId,
            );
            // ConsoleLogger(
            //   'ViewProductsActions >>>> fetchDeleteProductsAction >>>> CustomAsyncStorage.getDataFromStorage >>>> success_Data >>>> productItem: ',
            //   productItem,
            // );
            // ConsoleLogger(
            //   'ViewProductsActions >>>> fetchDeleteProductsAction >>>> CustomAsyncStorage.getDataFromStorage >>>> success_Data >>>> itemIndex: ',
            //   itemIndex,
            // );
            if (itemIndex !== -1) {
              available_Product_Data.splice(itemIndex, 1);
              CustomAsyncStorage.addDataToStorage(
                PRODUCT_DETAILS,
                JSON.stringify(available_Product_Data),
                (successData) => {
                  dispatchDeleteProducts(fetchGetProductsAction());
                  dispatchDeleteProducts(
                    deleteProductsSuccess('Data added successfully'),
                  );
                },
                (failureData) => {
                  dispatchDeleteProducts(deleteProductsFailure(failureData));
                },
              );
            } else {
              dispatchDeleteProducts(deleteProductsFailure('failureData'));
            }
          }
        },
        (failureData) => {
          dispatchDeleteProducts(deleteProductsFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchDeleteProducts(
        deleteProductsFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

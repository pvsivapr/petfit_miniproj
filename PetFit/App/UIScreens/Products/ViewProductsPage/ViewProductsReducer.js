import {
  GET_PRODUCTS,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCTS_FAILURE,
  DELETE_PRODUCTS,
  DELETE_PRODUCTS_SUCCESS,
  DELETE_PRODUCTS_FAILURE,
} from './ViewProductsActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';

const initialState = {
  productDetails: [],
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const ViewProductsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      state = {...state, loaderVisibility: true, serviceState: LOADING};
      break;
    case GET_PRODUCTS_SUCCESS:
      state = {
        ...state,
        productDetails: action.payload,
        serviceState: SUCCESS,
        loaderVisibility: false,
      };
      break;
    case GET_PRODUCTS_FAILURE:
      state = {
        ...state,
        formErrorMessage: action.payload,
        serviceState: FAILURE,
        loaderVisibility: false,
      };
    case DELETE_PRODUCTS:
      state = {...state, loaderVisibility: true, serviceState: LOADING};
      break;
    case DELETE_PRODUCTS_SUCCESS:
      state = {
        ...state,
        productDetails: action.payload,
        serviceState: SUCCESS,
        loaderVisibility: false,
      };
      break;
    case DELETE_PRODUCTS_FAILURE:
      state = {
        ...state,
        formErrorMessage: action.payload,
        serviceState: FAILURE,
        loaderVisibility: false,
      };
      break;
  }
  return state;
};

export default ViewProductsReducer;

import {
  CHECK_REPORT_ADD_PET_FORM_ERROR,
  UPDATE_ADD_PET_DETAILS,
  ADD_PET_ACCESS,
  ADD_PET_SUCCESS,
  ADD_PET_FAILURE,
  ADD_PET_FORM_RESET,
} from './AddPetActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';
import {ConsoleLogger} from './../../../BAL/EventLogger';

const initialState = {
  petDetails: {
    petId: '',
    petProfilePIC: '',
    petName: '',
    petDOB: '',
    petBreed: '',
    petLocation: '',
    petOwnerDetails: '',
    petImages: [],
    petVideos: [],
    petScheduleDetails: [],
  },
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const AddPetReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECK_REPORT_ADD_PET_FORM_ERROR:
      state = {
        ...state,
        serviceState: INACTIVE,
        formErrorMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case UPDATE_ADD_PET_DETAILS:
      state = {
        ...state,
        serviceState: INACTIVE,
        petDetails: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_PET_ACCESS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case ADD_PET_SUCCESS:
      state = {
        ...state,
        serviceState: SUCCESS,
        successMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_PET_FAILURE:
      state = {
        ...state,
        serviceState: FAILURE,
        errorMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_PET_FORM_RESET:
      state = initialState;
      break;
  }
  return state;
};

export default AddPetReducer;

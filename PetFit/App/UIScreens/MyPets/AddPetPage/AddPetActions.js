import {
  getAge,
  getDyanmicIDFromDate,
} from '../../../CommonComponents/GetDynamicID';
import {fetchGetPetsAction} from '../../HomePage/HomeActions';
import {ConsoleLogger, ErrorEventLogger} from './../../../BAL/EventLogger';
import {CustomAsyncStorage} from './../../../BAL/LocalDB';
import {PET_DETAILS} from './../../../BAL/LocalDB/LocalDBConstants';
import {
  ADD_PET_SUCCESS_MESSAGE,
  ADD_PET_FAILURE_MESSAGE,
} from './../../../Constants/TextConstants';

export const CHECK_REPORT_ADD_PET_FORM_ERROR =
  'CHECK_REPORT_ADD_PET_FORM_ERROR';
export const UPDATE_ADD_PET_DETAILS = 'UPDATE_ADD_PET_DETAILS';
export const ADD_PET_ACCESS = 'ADD_PET_ACCESS';
export const ADD_PET_SUCCESS = 'ADD_PET_SUCCESS';
export const ADD_PET_FAILURE = 'ADD_PET_FAILURE';
export const ADD_PET_FORM_RESET = 'ADD_PET_FORM_RESET';

export const fetchCheckReportAddPetFormError = (formErrorMessage) => ({
  type: CHECK_REPORT_ADD_PET_FORM_ERROR,
  payload: formErrorMessage,
});
export const fetchUpadteAddPetDetails = (petDetails) => ({
  type: UPDATE_ADD_PET_DETAILS,
  payload: petDetails,
});
export const fetchAddPetFormReset = () => ({
  type: ADD_PET_FORM_RESET,
});
export const getAddPetAccess = () => ({
  type: ADD_PET_ACCESS,
});
export const getAddPetSuccess = (successData) => ({
  type: ADD_PET_SUCCESS,
  payload: ADD_PET_SUCCESS_MESSAGE,
});
export const getAddPetFailure = (failureReport) => ({
  type: ADD_PET_FAILURE,
  payload: ADD_PET_FAILURE_MESSAGE,
});

export function fetchAddPetAction(petDetails) {
  return async (dispatchAddPet) => {
    dispatchAddPet(getAddPetAccess());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PET_DETAILS,
        (success_Data) => {
          ConsoleLogger(
            'AddPetDetails >>>> CustomAsyncStorage.getDataFromStorage >>>> success_Data',
            success_Data,
          );
          let available_Pets_Data = [];
          if (success_Data !== null && success_Data.response_data !== null) {
            available_Pets_Data = JSON.parse(success_Data.response_data);
          }
          let availablePetsData = available_Pets_Data;

          petDetails = {
            ...petDetails,
            petId: getDyanmicIDFromDate(),
            petAge: getAge(petDetails.petDOB),
          };
          availablePetsData.push(petDetails);

          CustomAsyncStorage.addDataToStorage(
            PET_DETAILS,
            JSON.stringify(availablePetsData),
            (successData) => {
              dispatchAddPet(fetchGetPetsAction());
              dispatchAddPet(getAddPetSuccess('Data added successfully'));
              dispatchAddPet(fetchAddPetFormReset());
            },
            (failureData) => {
              dispatchAddPet(getAddPetFailure(failureData));
            },
          );
        },
        (failureData) => {
          dispatchAddPet(getAddPetFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchAddPet(getAddPetFailure({errorMessage: 'Catch Block triggered'}));
    }
  };
}

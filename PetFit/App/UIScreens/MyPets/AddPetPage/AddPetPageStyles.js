import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import AppColors from './../../../Constants/Colors';
import {
  APP_MIN_INPUT_HEIGHT,
  APP_MIN_INPUT_WIDTH,
} from './../../../Constants/PageDimensions';
import {
  TEXT_LINK_COLOR,
  TEXT_INFO_COLOR,
} from './../../../Constants/AppStyleConstants';

const screenHeight = Dimensions.get('screen').height;
const screenWidth = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  textErrorInfoStyle: {
    color: 'maroon',
  },

  editorLegendTextStyle: {
    color: '#000000',
    fontSize: 20,
  },
  editorViewStyle: {
    minHeight: 150,
    height: 150,
    textAlignVertical: 'top',
  },

  petPhotoVideoTextStyle: {
    color: '#000000',
    flex: 1,
    fontSize: 20,
    alignSelf: 'center',
  },
  petPhotoVideoContainerStyle: {
    flexDirection: 'row',
    marginBottom: 20,
    marginTop: 10,
  },

  textInputHolderViewStyle: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingBottom: 10,
  },

  imageViewHolderStyle: {
    // marginLeft: (screenWidth - 150) / 2,
    // marginRight: (screenWidth - 150) / 2,
    marginTop: 20,
    marginBottom: 30,
    width: screenWidth - 20,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageViewStyle: {
    height: '100%',
    width: '100%',
    // aspectRatio: 1,
  },

  picturesHolderTextStyles: {
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  picturesHolderStyles: {
    flexDirection: 'row',
    backgroundColor: '#000000',
    marginTop: 'auto',
    marginBottom: 40,
  },

  buttonSendStyle: {
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
  },
  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgb(170,170,170)',
  },
});

export default styles;

import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './AddPetPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomButton from './../../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
import CustomTextInputField from './../../../CommonComponents/CustomTextInputField';
import {ConsoleLogger} from './../../../BAL/EventLogger';
import {
  PetDetailConstants,
  CAPTURE_PICTURE,
  CAPTURE_VIDEO,
  UPLOAD_PICTURE,
  UPLOAD_VIDEO,
} from './../../../Constants/PageNames';
import {
  PetDetailTextConstants,
  ADD_PET_BUTTON_TEXT,
  ADD_PET_TITLE,
} from './../../../Constants/TextConstants';
import {
  MASTER_HEADER_HAM_BURGER,
  PET_PIC_PLACEHOLDER,
  CAMERA_ICON,
  NEW_IMAGE_WHITE_ICON,
  IMAGE_GALLERY_WHITE_ICON,
  VIDEO_GALLERY_WHITE_ICON,
  NEW_VIDEO_WHITE_ICON,
} from './../../../Assets/ImageHelper';

import {
  fetchCheckReportAddPetFormError,
  fetchUpadteAddPetDetails,
  fetchAddPetAction,
  fetchAddPetFormReset,
} from './AddPetActions';
import CustomTouch from '../../../CommonComponents/CustomTouch';
import CustomModal from '../../../CommonComponents/CustomModal';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const AddPet_Page = (props) => {
  const {
    dispatch,
    loaderVisibility,
    petDetails,
    navigation,
    formErrorMessage,
  } = props;

  const [showMultimediaPopup, setShowMultimediaPopup] = useState(false);
  const [showProfilePicPopup, setShowProfilePicPopup] = useState(false);
  const [isProfilePicMode, setIsProfilePicMode] = useState(false);

  const imageOptions = {
    title: 'Select Avatar',
    customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  const videoOptions = {
    title: 'Video Picker',
    takePhotoButtonTitle: 'Take Video...',
    mediaType: 'video',
    videoQuality: 'low',
  };

  useEffect(() => {
    dispatch(fetchAddPetFormReset());
  }, []);

  const onNavigationButtonPresshandler = () => {
    navigation.toggleDrawer();
  };

  const onPressAddPet = async () => {
    ConsoleLogger('AddPetPage >>>> onPressLogin >>>> props: ', props);
    if (petDetails !== null && petDetails !== undefined) {
      if (
        petDetails.petName === null ||
        petDetails.petName === undefined ||
        petDetails.petName === ''
      ) {
        dispatch(
          fetchCheckReportAddPetFormError('Please enter a valid pet name'),
        );
      } else if (
        petDetails.petDOB === null ||
        petDetails.petDOB === undefined ||
        petDetails.petDOB === ''
      ) {
        dispatch(
          fetchCheckReportAddPetFormError('Please enter a valid password'),
        );
      } else if (
        petDetails.petBreed === null ||
        petDetails.petBreed === undefined ||
        petDetails.petBreed === ''
      ) {
        dispatch(fetchCheckReportAddPetFormError('Please enter pet breed'));
      } else if (
        petDetails.petLocation === null ||
        petDetails.petLocation === undefined ||
        petDetails.petLocation === ''
      ) {
        dispatch(
          fetchCheckReportAddPetFormError("Please enter a pet's location"),
        );
      } else if (
        petDetails.petOwnerDetails === null ||
        petDetails.petOwnerDetails === undefined ||
        petDetails.petOwnerDetails === ''
      ) {
        dispatch(
          fetchCheckReportAddPetFormError('Please enter a owners details'),
        );
      } else {
        dispatch(fetchAddPetAction(petDetails));
      }
    } else {
      dispatch(
        fetchCheckReportAddPetFormError('Please fill all mandatory fields'),
      );
    }
  };

  const onTextChangedHandler = (text, inputID) => {
    let pet_Details = petDetails;
    let isupdated = false;
    if (inputID === PetDetailConstants.PET_NAME) {
      pet_Details = {...petDetails, petName: text};
      isupdated = true;
    } else if (inputID === PetDetailConstants.PET_DOB) {
      pet_Details = {...petDetails, petDOB: text};
      isupdated = true;
    } else if (inputID === PetDetailConstants.PET_BREED) {
      pet_Details = {...petDetails, petBreed: text};
      isupdated = true;
    } else if (inputID === PetDetailConstants.PET_LOCATION) {
      pet_Details = {...petDetails, petLocation: text};
      isupdated = true;
    } else if (inputID === PetDetailConstants.PET_OWNER_DETAILS) {
      pet_Details = {...petDetails, petOwnerDetails: text};
      isupdated = true;
    } else {
      pet_Details = {...petDetails};
    }
    if (isupdated) {
      dispatch(fetchUpadteAddPetDetails(pet_Details));
    }
  };

  const picturesItems = [
    {
      multimediaId: CAPTURE_PICTURE,
      image: NEW_IMAGE_WHITE_ICON,
      title: 'Take Picture',
    },
    {
      multimediaId: CAPTURE_VIDEO,
      image: NEW_VIDEO_WHITE_ICON,
      title: 'Video Rec',
    },
    {
      multimediaId: UPLOAD_PICTURE,
      image: IMAGE_GALLERY_WHITE_ICON,
      title: 'Pic Gallery',
    },
    {
      multimediaId: UPLOAD_VIDEO,
      image: VIDEO_GALLERY_WHITE_ICON,
      title: 'Video Gallery',
    },
  ];

  const onCameraVideoIconClickHandler = () => {
    setShowMultimediaPopup(true);
  };

  const onImageUri = (uriOptions) => {
    const {imagePath} = uriOptions;
    if (
      imagePath !== null &&
      imagePath !== undefined &&
      imagePath !== '' &&
      imagePath !== ' '
    ) {
      let pet_Details = {};
      if (isProfilePicMode) {
        pet_Details = {...petDetails, petProfilePIC: imagePath};
      } else {
        let availableImages = petDetails.petImages;
        if (
          availableImages === null ||
          availableImages === undefined ||
          availableImages === '' ||
          availableImages === ' '
        ) {
          availableImages = [];
        }
        availableImages.push(imagePath);
        pet_Details = {...petDetails, petImages: availableImages};
      }
      dispatch(fetchUpadteAddPetDetails(pet_Details));
    }
  };

  const onVideoUri = (uriOptions) => {
    const {videoPath} = uriOptions;
    if (
      videoPath !== null &&
      videoPath !== undefined &&
      videoPath !== '' &&
      videoPath !== ' '
    ) {
      let pet_Details = petDetails;
      let availableVideo = pet_Details.petVideos;
      if (
        availableVideo === null ||
        availableVideo === undefined ||
        availableVideo === '' ||
        availableVideo === ' '
      ) {
        availableVideo = [];
      }
      availableVideo.push(videoPath);
      pet_Details = {...pet_Details, petVideos: availableVideo};
      dispatch(fetchUpadteAddPetDetails(pet_Details));
    }
  };

  const onMultimediaClickHandler = (item) => {
    const {multimediaId} = item;
    if (multimediaId === CAPTURE_PICTURE) {
      launchCamera(imageOptions, (response) => {
        onCustomModalClickHandler();
        const {uri} = response;
        onImageUri({
          imagePath: uri,
          selectedImageDetails: response,
        });
      });
    } else if (multimediaId === CAPTURE_VIDEO) {
      launchCamera(videoOptions, (response) => {
        onCustomModalClickHandler();
        const {uri} = response;
        onVideoUri({
          videoPath: uri,
          selectedVideoDetails: response,
        });
      });
    } else if (multimediaId === UPLOAD_PICTURE) {
      launchImageLibrary(imageOptions, (response) => {
        onCustomModalClickHandler();
        const {uri} = response;
        onImageUri({
          imagePath: uri,
          selectedImageDetails: response,
        });
      });
    } else if (multimediaId === UPLOAD_VIDEO) {
      launchCamera(videoOptions, (response) => {
        onCustomModalClickHandler();
        const {uri} = response;
        onVideoUri({
          videoPath: uri,
          selectedVideoDetails: response,
        });
      });
    } else {
    }
  };

  const onCustomModalClickHandler = () => {
    if (isProfilePicMode === true) {
      setShowProfilePicPopup(false);
      setIsProfilePicMode(false);
    } else {
      setShowMultimediaPopup(false);
    }
  };

  const galleryViews = (multimediaItem) => {
    const galleryUIView = (
      <CustomTouch
        childData={multimediaItem}
        onPress={onMultimediaClickHandler}>
        <Image source={multimediaItem.image} />
        <Text style={styles.picturesHolderTextStyles}>
          {multimediaItem.title}
        </Text>
      </CustomTouch>
    );
    return galleryUIView;
  };

  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        <CustomActivityIndicator visibility={loaderVisibility} />

        {showMultimediaPopup === true ? (
          <CustomModal onPress={() => onCustomModalClickHandler()}>
            <View style={styles.picturesHolderStyles}>
              {picturesItems.map((item) => {
                return galleryViews(item);
              })}
            </View>
          </CustomModal>
        ) : (
          <></>
        )}

        {showProfilePicPopup === true ? (
          <CustomModal onPress={() => onCustomModalClickHandler()}>
            <View style={styles.picturesHolderStyles}>
              {galleryViews(picturesItems[0])}
              {galleryViews(picturesItems[2])}
            </View>
          </CustomModal>
        ) : (
          <></>
        )}

        <CustomHeader
          title={ADD_PET_TITLE}
          showNavigationIcon={true}
          isMasterDetailHeader={true}
          navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          onNavigationButtonPress={onNavigationButtonPresshandler}
        />

        <ScrollView>
          <View style={styles.textInputHolderViewStyle}>
            <CustomTouch
              onPress={() => {
                setShowProfilePicPopup(true);
                setIsProfilePicMode(true);
                //onProfilePicClickHandler;
              }}>
              <View style={styles.imageViewHolderStyle}>
                <Image
                  style={styles.imageViewStyle}
                  source={
                    petDetails.petProfilePIC !== null &&
                    petDetails.petProfilePIC !== undefined &&
                    petDetails.petProfilePIC !== ''
                      ? {uri: petDetails.petProfilePIC}
                      : PET_PIC_PLACEHOLDER
                  }
                />
              </View>
            </CustomTouch>
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={PetDetailConstants.PET_NAME}
              isFullEntry={false}
              legendTitle={PetDetailTextConstants.PET_NAME_LEGEND_TEXT}
              placeHolder={PetDetailTextConstants.PET_NAME_PLACEHOLDER_TEXT}
              hintText={PetDetailTextConstants.PET_NAME_ERROR_HINT_TEXT}
              value={petDetails.petName}
            />
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={PetDetailConstants.PET_DOB}
              isFullEntry={false}
              legendTitle={PetDetailTextConstants.PET_DOB_LEGEND_TEXT}
              placeHolder={PetDetailTextConstants.PET_DOB_PLACEHOLDER_TEXT}
              hintText={PetDetailTextConstants.PET_DOB_ERROR_HINT_TEXT}
              value={petDetails.petDOB}
            />
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={PetDetailConstants.PET_BREED}
              isFullEntry={false}
              legendTitle={PetDetailTextConstants.PET_BREED_LEGEND_TEXT}
              placeHolder={PetDetailTextConstants.PET_BREED_PLACEHOLDER_TEXT}
              hintText={PetDetailTextConstants.PET_BREED_ERROR_HINT_TEXT}
              value={petDetails.petBreed}
            />
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={PetDetailConstants.PET_LOCATION}
              isFullEntry={false}
              legendTitle={PetDetailTextConstants.PET_LOCATION_LEGEND_TEXT}
              placeHolder={PetDetailTextConstants.PET_LOCATION_PLACEHOLDER_TEXT}
              hintText={PetDetailTextConstants.PET_LOCATION_ERROR_HINT_TEXT}
              value={petDetails.petLocation}
            />
            <Text style={styles.editorLegendTextStyle}>
              {PetDetailTextConstants.PET_OWNER_DETAILS_LEGEND_TEXT}
            </Text>
            <CustomTextInputField
              multiline={true}
              fontStyle={styles.editorViewStyle}
              onChangeText={onTextChangedHandler}
              inputID={PetDetailConstants.PET_OWNER_DETAILS}
              legendTitle={PetDetailTextConstants.PET_OWNER_DETAILS_LEGEND_TEXT}
              placeHolder={
                PetDetailTextConstants.PET_OWNER_DETAILS_PLACEHOLDER_TEXT
              }
              hintText={
                PetDetailTextConstants.PET_OWNER_DETAILS_ERROR_HINT_TEXT
              }
              value={petDetails.petOwnerDetails}
            />
            <View style={styles.petPhotoVideoContainerStyle}>
              <Text style={styles.petPhotoVideoTextStyle}>
                {"Add Pet's Photo/Video"}
              </Text>
              <CustomTouch onPress={onCameraVideoIconClickHandler}>
                <Image source={CAMERA_ICON} />
              </CustomTouch>
            </View>
            {formErrorMessage !== null &&
            formErrorMessage !== undefined &&
            formErrorMessage !== '' ? (
              <Text style={styles.textErrorInfoStyle}>{formErrorMessage}</Text>
            ) : (
              <></>
            )}

            <CustomButton
              title={ADD_PET_BUTTON_TEXT}
              style={styles.buttonSendStyle}
              onPress={onPressAddPet}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    petDetails: state.AddPetReducer.petDetails,
    serviceState: state.AddPetReducer.serviceState,
    loaderVisibility: state.AddPetReducer.loaderVisibility,
    errorMessage: state.AddPetReducer.errorMessage,
    successMessage: state.AddPetReducer.successMessage,
    formErrorMessage: state.AddPetReducer.formErrorMessage,
  };
};

const AddPetPage = connect(mapStateToProps)(AddPet_Page);

export default AddPetPage;

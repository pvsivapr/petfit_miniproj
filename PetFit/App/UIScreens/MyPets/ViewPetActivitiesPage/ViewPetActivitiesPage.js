import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  FlatList,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './ViewPetActivitiesPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
import {ConsoleLogger} from './../../../BAL/EventLogger';
import {ADD_PET_ACTIVITY_TITLE} from './../../../Constants/TextConstants';
import {DELETE_ICON} from './../../../Assets/ImageHelper';
import {
  fetchGetPetActivityAction,
  fetchDeletePetSchedulesAction,
} from './ViewPetActivitiesActions';
import CustomTouch from '../../../CommonComponents/CustomTouch';

const ViewPetActivities_Page = (props) => {
  const {
    route,
    dispatch,
    petScheduleDetails,
    loaderVisibility,
    formErrorMessage,
    navigation,
  } = props;

  useEffect(() => {
    const petId = route.params.petId;
    dispatch(fetchGetPetActivityAction(petId));
  }, []);

  const onNavigationButtonPresshandler = () => {
    navigation.pop();
  };

  const deleteScheduleClickHandler = (activityId) => {
    const deleteDetails = {
      petId: route.params.petId,
      activityId: activityId,
    };
    dispatch(fetchDeletePetSchedulesAction(deleteDetails));
  };

  const flatListItemView = ({item}) => {
    // const customItem = {
    //   petDetails: item,
    //   navigation: this.props.navigation,
    // };
    return (
      <View style={styles.flatListHolderStyle}>
        <View style={styles.flatListItemTextHolderStyle}>
          <Text style={{...styles.flatListItemTextStyle, fontWeight: 'bold'}}>
            {item.activityTitle}
          </Text>
          <Text style={styles.flatListItemTextStyle}>{item.activityNotes}</Text>
          <Text style={styles.flatListItemTextStyle}>{item.petLocation}</Text>
        </View>
        <CustomTouch
          childData={item.activityId}
          onPress={deleteScheduleClickHandler}>
          <Image style={styles.flatListItemImageStyle} source={DELETE_ICON} />
        </CustomTouch>
      </View>
    );
  };
  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        {/* <View style={styles.mainViewStyle}> */}
        <CustomActivityIndicator visibility={loaderVisibility} />

        <CustomHeader
          title={ADD_PET_ACTIVITY_TITLE}
          showNavigationIcon={true}
          isMasterDetailHeader={false}
          //   navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          onNavigationButtonPress={onNavigationButtonPresshandler}
        />
        {
          <View style={styles.flatListContainerStyle}>
            <FlatList
              data={petScheduleDetails}
              renderItem={flatListItemView}
              keyExtractor={(item) => item.id}
            />
          </View>
        }
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    petScheduleDetails: state.ViewPetActivitiesReducer.petScheduleDetails,
    serviceState: state.ViewPetActivitiesReducer.serviceState,
    loaderVisibility: state.ViewPetActivitiesReducer.loaderVisibility,
    errorMessage: state.ViewPetActivitiesReducer.errorMessage,
    successMessage: state.ViewPetActivitiesReducer.successMessage,
    formErrorMessage: state.ViewPetActivitiesReducer.formErrorMessage,
  };
};

const ViewPetActivitiesPage = connect(mapStateToProps)(ViewPetActivities_Page);

export default ViewPetActivitiesPage;

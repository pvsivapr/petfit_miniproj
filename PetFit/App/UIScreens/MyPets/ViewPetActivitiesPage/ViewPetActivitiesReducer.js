import {
  GET_PET_SCHEDULES,
  GET_PET_SCHEDULES_SUCCESS,
  GET_PET_SCHEDULES_FAILURE,
  DELETE_PET_SCHEDULES,
  DELETE_PET_SCHEDULES_SUCCESS,
  DELETE_PET_SCHEDULES_FAILURE,
} from './ViewPetActivitiesActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';

const initialState = {
  petScheduleDetails: [],
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const ViewPetActivitiesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PET_SCHEDULES:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case GET_PET_SCHEDULES_SUCCESS:
      state = {
        ...state,
        serviceState: SUCCESS,
        loaderVisibility: false,
        petScheduleDetails: action.payload,
      };
      break;
    case GET_PET_SCHEDULES_FAILURE:
      state = {
        ...state,
        serviceState: FAILURE,
        loaderVisibility: false,
      };
      break;
    case DELETE_PET_SCHEDULES:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case DELETE_PET_SCHEDULES_SUCCESS:
      state = {
        ...state,
        serviceState: SUCCESS,
        loaderVisibility: false,
      };
      break;
    case DELETE_PET_SCHEDULES_FAILURE:
      state = {
        ...state,
        serviceState: FAILURE,
        loaderVisibility: false,
      };
      break;
  }
  return state;
};

export default ViewPetActivitiesReducer;

import {ConsoleLogger, ErrorEventLogger} from '../../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {PET_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';
import {fetchGetPetsAction} from '../../HomePage/HomeActions';

export const GET_PET_SCHEDULES = 'GET_PET_SCHEDULES';
export const GET_PET_SCHEDULES_SUCCESS = 'GET_PET_SCHEDULES_SUCCESS';
export const GET_PET_SCHEDULES_FAILURE = 'GET_PET_SCHEDULES_FAILURE';
export const DELETE_PET_SCHEDULES = 'DELETE_PET_SCHEDULES';
export const DELETE_PET_SCHEDULES_SUCCESS = 'DELETE_PET_SCHEDULES_SUCCESS';
export const DELETE_PET_SCHEDULES_FAILURE = 'DELETE_PET_SCHEDULES_FAILURE';

export const getPetSchedules = () => ({
  type: GET_PET_SCHEDULES,
});

export const getPetSchedulesSuccess = (petSchedules) => ({
  type: GET_PET_SCHEDULES_SUCCESS,
  payload: petSchedules,
});

export const getPetSchedulesFailure = () => ({
  type: GET_PET_SCHEDULES_FAILURE,
});
export const deletePetSchedules = () => ({
  type: DELETE_PET_SCHEDULES,
});

export const deletePetSchedulesSuccess = (productsData) => ({
  type: DELETE_PET_SCHEDULES_SUCCESS,
  payload: productsData,
});

export const deletePetSchedulesFailure = (failureData) => ({
  type: DELETE_PET_SCHEDULES_FAILURE,
  payload: failureData,
});

export function fetchGetPetActivityAction(petId) {
  return async (dispatchViewPetActivities) => {
    dispatchViewPetActivities(getPetSchedules());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PET_DETAILS,
        (success_Data) => {
          let available_Pets_Data = null;
          if (success_Data !== null && success_Data.response_data !== null) {
            available_Pets_Data = JSON.parse(success_Data.response_data);
            const itemIndex = available_Pets_Data.findIndex(
              (item) => item.petId === petId,
            );
            if (itemIndex !== -1) {
              let obtainedPetItem = available_Pets_Data[itemIndex];
              dispatchViewPetActivities(
                getPetSchedulesSuccess(obtainedPetItem.petScheduleDetails),
              );
            }
          }
        },
        (failureData) => {
          dispatchViewPetActivities(getPetSchedulesFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchViewPetActivities(
        getPetSchedulesFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

export function fetchDeletePetSchedulesAction(deleteItemDetails) {
  return async (dispatchDeletePetSchedules) => {
    dispatchDeletePetSchedules(deletePetSchedules());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PET_DETAILS,
        (success_Data) => {
          if (success_Data !== null && success_Data.response_data !== null) {
            let available_Pets_Data = JSON.parse(success_Data.response_data);
            const petItemIndex = available_Pets_Data.findIndex(
              (item) => item.petId === deleteItemDetails.petId,
            );
            if (petItemIndex !== -1) {
              let obtainedPetItem = available_Pets_Data[petItemIndex];
              let availablePetSchedules = obtainedPetItem.petScheduleDetails;

              const scheduleItemIndex = availablePetSchedules.findIndex(
                (item) => item.activityId === deleteItemDetails.activityId,
              );
              // ConsoleLogger(
              //   'ViewPetSchedulesActions >>>> fetchDeletePetSchedulesAction >>>> scheduleItemIndex: ',
              //   scheduleItemIndex,
              // );
              // dispatchDeletePetSchedules(deletePetSchedulesFailure());

              if (scheduleItemIndex !== -1) {
                availablePetSchedules.splice(scheduleItemIndex, 1);
                obtainedPetItem.petScheduleDetails = availablePetSchedules;
                available_Pets_Data[petItemIndex] = obtainedPetItem;

                CustomAsyncStorage.addDataToStorage(
                  PET_DETAILS,
                  JSON.stringify(available_Pets_Data),
                  (successData) => {
                    dispatchDeletePetSchedules(
                      deletePetSchedulesSuccess('Data added successfully'),
                    );
                    dispatchDeletePetSchedules(
                      fetchGetPetActivityAction(deleteItemDetails.petId),
                    );
                    dispatchDeletePetSchedules(fetchGetPetsAction());
                  },
                  (failureData) => {
                    dispatchDeletePetSchedules(deletePetSchedulesFailure());
                  },
                );
              }
            } else {
              dispatchDeletePetSchedules(
                deletePetSchedulesFailure('failureData'),
              );
            }
          }
        },
        (failureData) => {
          dispatchDeletePetSchedules(deletePetSchedulesFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchDeletePetSchedules(
        deletePetSchedulesFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

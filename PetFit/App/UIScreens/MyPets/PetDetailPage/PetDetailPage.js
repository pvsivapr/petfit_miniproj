import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './PetDetailPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomButton from './../../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
import {
  PetDetailConstants,
  PET_PICTURES_PAGE,
  SUCCESS,
} from './../../../Constants/PageNames';
import {
  PetDetailTextConstants,
  PET_DETAIL_TITLE,
  CALL_OWNER_BUTTON_TEXT,
  DELETE_PET_BUTTON_TEXT,
} from './../../../Constants/TextConstants';
import {
  SCHEDULE_PET_ACTIVITY_PAGE,
  VIEW_PET_ACTIVITIES_PAGE,
  PET_ALBUM_PAGE,
  PET_ALBUM_NAVIGATOR,
} from './../../../Constants/PageNames';
import {
  PET_PIC_PLACEHOLDER,
  CAMERA_ICON,
  CALENDAR_ICON,
  TODO_ICON,
} from './../../../Assets/ImageHelper';
import CustomTextField from '../../../CommonComponents/CustomTextFiled';
import {ConsoleLogger} from '../../../BAL/EventLogger';
import CustomTouch from '../../../CommonComponents/CustomTouch';
import {fetchDeletePetsAction, fetchDeletePetsReset} from './PetDetailActions';

const PetDetail_Page = (props) => {
  const {
    route,
    dispatch,
    serviceState,
    loaderVisibility,
    navigation,
    formErrorMessage,
  } = props;

  // useEffect(() => {
  //   if (serviceState === SUCCESS) {
  //     dispatch(fetchDeletePetsReset());
  //     navigation.pop();
  //     popBack(true);
  //   }
  // }, [dispatch, navigation, popBack, serviceState]);

  useEffect(() => {
    if (serviceState === SUCCESS) {
      dispatch(fetchDeletePetsReset());
      navigation.pop();
    }
  }, [dispatch, navigation, serviceState]);

  // if (serviceState === SUCCESS) {
  //   navigation.pop();
  // }

  const petDetails = route.params.petDetails;

  const onCallToOwnerClickHandler = async () => {};

  const onDeletePetClickHandler = async () => {
    dispatch(fetchDeletePetsAction(petDetails));
  };

  const onCameraVideoDisplayClickHandler = async () => {
    // props.navigation.navigate(PET_ALBUM_NAVIGATOR, {
    //   petDetails: petDetails,
    // });
    props.navigation.navigate(PET_PICTURES_PAGE, {
      petImages: petDetails.petImages,
    });
  };

  const onCreateScheduleHandler = async () => {
    props.navigation.navigate(SCHEDULE_PET_ACTIVITY_PAGE, {
      petId: petDetails.petId,
    });
  };

  const onViewScheduleHandler = async () => {
    props.navigation.navigate(VIEW_PET_ACTIVITIES_PAGE, {
      petId: petDetails.petId,
    });
  };

  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        <CustomActivityIndicator visibility={loaderVisibility} />

        <CustomHeader
          title={PET_DETAIL_TITLE}
          showNavigationIcon={true}
          navigation={props.navigation}
        />
        <ScrollView>
          <View style={styles.imageViewHolderStyle}>
            {petDetails.petProfilePIC !== null &&
            petDetails.petProfilePIC !== undefined &&
            petDetails.petProfilePIC !== '' ? (
              <Image
                style={styles.imageViewStyle}
                source={{uri: petDetails.petProfilePIC}}
              />
            ) : (
              <>
                <Image
                  style={styles.imageViewStyle}
                  source={PET_PIC_PLACEHOLDER}
                />
              </>
            )}
          </View>
          <View style={styles.textInputHolderViewStyle}>
            <CustomTextField
              inputID={PetDetailConstants.PET_NAME}
              isFullDisplay={false}
              legendTitle={PetDetailTextConstants.PET_NAME_LEGEND_TEXT}
              value={petDetails.petName}
            />
            <CustomTextField
              inputID={PetDetailConstants.PET_DOB}
              isFullDisplay={false}
              legendTitle={PetDetailTextConstants.PET_DOB_LEGEND_TEXT}
              value={petDetails.petAge}
            />
            <CustomTextField
              inputID={PetDetailConstants.PET_BREED}
              isFullDisplay={false}
              legendTitle={PetDetailTextConstants.PET_BREED_LEGEND_TEXT}
              value={petDetails.petBreed}
            />
            <CustomTextField
              inputID={PetDetailConstants.PET_LOCATION}
              isFullDisplay={false}
              legendTitle={PetDetailTextConstants.PET_LOCATION_LEGEND_TEXT}
              value={petDetails.petLocation}
            />
            <Text style={styles.editorLegendTextStyle}>
              {PetDetailTextConstants.PET_OWNER_DETAILS_LEGEND_TEXT}
            </Text>
            <CustomTextField
              multiline={true}
              style={{backgroundColor: '#FFFFFF'}}
              fontStyle={styles.editorViewStyle}
              inputID={PetDetailConstants.PET_OWNER_DETAILS}
              value={petDetails.petOwnerDetails}
            />
            {formErrorMessage !== null &&
            formErrorMessage !== undefined &&
            formErrorMessage !== '' ? (
              <Text style={styles.textErrorInfoStyle}>{formErrorMessage}</Text>
            ) : (
              <></>
            )}
          </View>
          <View style={{marginHorizontal: 10}}>
            <View style={styles.petPhotoVideoContainerStyle}>
              <Text style={styles.petPhotoVideoTextStyle}>{'Album'}</Text>
              <CustomTouch onPress={onCameraVideoDisplayClickHandler}>
                <Image source={CAMERA_ICON} />
              </CustomTouch>
            </View>

            <View style={styles.petPhotoVideoContainerStyle}>
              <Text style={styles.petPhotoVideoTextStyle}>{'Schedule'}</Text>
              <CustomTouch onPress={onCreateScheduleHandler}>
                <Image source={CALENDAR_ICON} />
              </CustomTouch>
              <CustomTouch onPress={onViewScheduleHandler}>
                <Image source={TODO_ICON} />
              </CustomTouch>
            </View>
          </View>
        </ScrollView>
        <View style={styles.buttonHolderStyle}>
          <CustomButton
            title={CALL_OWNER_BUTTON_TEXT}
            style={styles.buttonSendStyle}
            fontStyle={styles.buttonSendFontStyle}
            onPress={onCallToOwnerClickHandler}
          />
          <CustomButton
            title={DELETE_PET_BUTTON_TEXT}
            style={styles.buttonSendStyle}
            fontStyle={styles.buttonSendFontStyle}
            onPress={onDeletePetClickHandler}
          />
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    serviceState: state.PetDetailReducer.serviceState,
    loaderVisibility: state.PetDetailReducer.loaderVisibility,
    errorMessage: state.PetDetailReducer.errorMessage,
    successMessage: state.PetDetailReducer.successMessage,
    formErrorMessage: state.PetDetailReducer.formErrorMessage,
  };
};

const PetDetailPage = connect(mapStateToProps)(PetDetail_Page);

export default PetDetailPage;

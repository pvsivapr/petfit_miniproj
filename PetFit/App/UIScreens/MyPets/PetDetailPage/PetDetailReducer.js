import {
  DELETE_PETS,
  DELETE_PETS_SUCCESS,
  DELETE_PETS_FAILURE,
  DELETE_PETS_RESET,
} from './PetDetailActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';

const initialState = {
  // petDetails: {
  //   petId: '',
  //   petName: '',
  //   petDOB: '',
  //   petAge: '',
  //   petLocation: '',
  //   petBreed: '',
  //   petOwnerDetails: '',
  //   petProfilePIC: '',
  //   petImages: [{}],
  //   petVideos: [{}],
  // },
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const PetDetailReducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_PETS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case DELETE_PETS_SUCCESS:
      state = {...state, serviceState: SUCCESS, loaderVisibility: false};
      break;
    case DELETE_PETS_FAILURE:
      state = {...state, serviceState: FAILURE, loaderVisibility: false};
      break;
      case DELETE_PETS_RESET:
        state = {...state, serviceState: INACTIVE, loaderVisibility: false};
        break;
  }
  return state;
};

export default PetDetailReducer;

// import {ErrorEventLogger} from './../../../BAL/EventLogger';
// import {CustomAsyncStorage, ExecuteDBOperations} from './../../../BAL/LocalDB';
// import {PET_DETAILS} from './../../../BAL/LocalDB/LocalDBConstants';
import {ConsoleLogger, ErrorEventLogger} from '../../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {PET_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';
import {fetchGetPetsAction} from '../../HomePage/HomeActions';
// import {
//   ADD_PET_SUCCESS_MESSAGE,
//   ADD_PET_FAILURE_MESSAGE,
// } from './../../../Constants/TextConstants';

export const DELETE_PETS = 'DELETE_PETS';
export const DELETE_PETS_SUCCESS = 'DELETE_PETS_SUCCESS';
export const DELETE_PETS_FAILURE = 'DELETE_PETS_FAILURE';
export const DELETE_PETS_RESET = 'DELETE_PETS_RESET';

export const deletePets = () => ({
  type: DELETE_PETS,
});

export const deletePetsSuccess = (productsData) => ({
  type: DELETE_PETS_SUCCESS,
  payload: productsData,
});

export const deletePetsFailure = (failureData) => ({
  type: DELETE_PETS_FAILURE,
  payload: failureData,
});

export const fetchDeletePetsReset = () => ({
  type: DELETE_PETS_RESET,
});

export function fetchDeletePetsAction(petItem) {
  return async (dispatchDeletePets) => {
    dispatchDeletePets(deletePets());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PET_DETAILS,
        (success_Data) => {
          if (success_Data !== null && success_Data.response_data !== null) {
            let available_Pet_Data = JSON.parse(success_Data.response_data);
            // const itemIndex = available_Product_Data.indexOf((item) => item.productId ==);

            const itemIndex = available_Pet_Data.findIndex(
              (item) => item.petId === petItem.petId,
            );
            ConsoleLogger(
              'PetDetailActions >>>> fetchDeletePetsAction >>>> CustomAsyncStorage.getDataFromStorage >>>> success_Data >>>> petItem: ',
              petItem,
            );
            ConsoleLogger(
              'PetDetailActions >>>> fetchDeletePetsAction >>>> CustomAsyncStorage.getDataFromStorage >>>> success_Data >>>> itemIndex: ',
              itemIndex,
            );
            if (itemIndex !== -1) {
              available_Pet_Data.splice(itemIndex, 1);
              CustomAsyncStorage.addDataToStorage(
                PET_DETAILS,
                JSON.stringify(available_Pet_Data),
                (successData) => {
                  dispatchDeletePets(fetchGetPetsAction());
                  dispatchDeletePets(
                    deletePetsSuccess('Data added successfully'),
                  );
                },
                (failureData) => {
                  dispatchDeletePets(deletePetsFailure(failureData));
                },
              );
            } else {
              dispatchDeletePets(deletePetsFailure('failureData'));
            }
          }
        },
        (failureData) => {
          dispatchDeletePets(deletePetsFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchDeletePets(
        deletePetsFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

export const DELETE_PET = 'DELETE_PET';

export const fetchPetDelete = () => ({
  type: DELETE_PET,
});
// export const fetchUpadteAddPetDetails = (petDetails) => ({
//   type: UPDATE_ADD_PET_DETAILS,
//   payload: petDetails,
// });

// export function fetchAddPetAction(petDetails) {
//   return async (dispatchAddPet) => {
//     dispatchAddPet(getAddPetAccess());
//     try {
//       // ExecuteDBOperations.addDataToStorage(_PROFILE_DETAILS, JSON.stringify(Details), (successData) => {
//       //   dispatchAddPet(getAddPetSuccess(successData));
//       // }, (failureData) => {
//       //   dispatchAddPet(getAddPetFailure(failureData));
//       // });
//     } catch (error) {
//       ErrorEventLogger(error);
//       dispatchAddPet(getAddPetFailure({errorMessage: 'Catch Block triggered'}));
//     }
//   };
// }

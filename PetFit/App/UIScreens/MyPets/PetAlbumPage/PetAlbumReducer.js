import {INACTIVE} from './../../../Constants/PageNames';

const initialState = {
  petImages: [{}],
  petVideos: [{}],
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const PetAlbumReducer = (state = initialState) => {
  state = {
    ...state,
    petImages: [{}],
    petVideos: [{}],
    serviceState: INACTIVE,
    loaderVisibility: false,
    successMessage: '',
    errorMessage: '',
    formErrorMessage: '',
  };
  return state;
};

export default PetAlbumReducer;

import React from 'react';
import {
  View,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
  StyleSheet,
  Dimensions,
} from 'react-native';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import {PET_ALUBM_PAGE_TITLE} from './../../../Constants/TextConstants';

const screenWidth = Dimensions.get('screen').width;
const screenHeight = Dimensions.get('screen').height;

const PetPictures_Page = (props) => {
  const {route} = props;
  const petImages = route.params.petImages;
  const numberOfRowsArray = [];
  const isEven = petImages % 2 === 0 ? true : false;
  var numberOfRows = 0;
  if (isEven) {
    numberOfRows = petImages.length / 2;
  } else {
    numberOfRows = (petImages.length - 1) / 2 + 1;
  }
  if (numberOfRows !== -1) {
    for (var i = 1; i <= numberOfRows; i++) {
      numberOfRowsArray.push(i);
    }
  }

  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        <CustomHeader
          title={PET_ALUBM_PAGE_TITLE}
          showNavigationIcon={true}
          navigation={props.navigation}
        />

        <ScrollView>
          <View style={styles.imageViewHolderStyle}>
            {numberOfRowsArray.map((item) => {
              return (
                <View style={styles.imagesHolder}>
                  <Image
                    style={styles.imagesDisplayStyle}
                    source={{
                      uri: petImages[2 * item - 2],
                    }}
                  />
                  <Image
                    style={styles.imagesDisplayStyle}
                    source={{
                      uri: petImages[2 * item - 1],
                    }}
                  />
                </View>
              );
            })}
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};

const PetPicturesPage = PetPictures_Page;

export default PetPicturesPage;

const styles = StyleSheet.create({
  imagesDisplayStyle: {
    width: (screenWidth / 100) * 50 - 20,
    marginHorizontal: 10,
    height: (screenHeight / 100) * 25 - 20,
    marginVertical: 5,
  },
  imagesHolder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imagesListHolder: {},

  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgb(170,170,170)',
  },
});

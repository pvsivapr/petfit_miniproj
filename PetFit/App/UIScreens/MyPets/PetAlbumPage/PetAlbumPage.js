import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './PetAlbumPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomButton from './../../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
// import {ConsoleLogger} from './../../../BAL/EventLogger';
import {PetDetailConstants} from './../../../Constants/PageNames';
import {
  PetDetailTextConstants,
  PET_ALUBM_PAGE_TITLE,
  PET_PICTURES_TAB_TITLE_TEXT,
  PET_VIDEOS_TAB_TITLE_TEXT,
} from './../../../Constants/TextConstants';
import {
  SCHEDULE_PET_ACTIVITY_PAGE,
  VIEW_PET_ACTIVITIES_PAGE,
} from './../../../Constants/PageNames';
import {
  PET_PIC_PLACEHOLDER,
  CAMERA_ICON,
  CALENDAR_ICON,
  TODO_ICON,
} from './../../../Assets/ImageHelper';
import CustomTextField from '../../../CommonComponents/CustomTextFiled';
import {ConsoleLogger} from '../../../BAL/EventLogger';
import CustomTouch from '../../../CommonComponents/CustomTouch';
// import PetAlbumNavigator from '../../../Navigation/PetAlbumNavigator';

const PetAlbum_Page = (props) => {
  const {route, loaderVisibility, formErrorMessage} = props;
  const items = route.params.petImages;

  

  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        {/* <View style={styles.mainViewStyle}> */}
        {/* <CustomActivityIndicator visibility={loaderVisibility} /> */}

        <CustomHeader
          title={PET_ALUBM_PAGE_TITLE}
          showNavigationIcon={true}
          // isMasterDetailHeader={true}
          // navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          // onNavigationButtonPress={onNavigationButtonPresshandler}
        />
        {/* <PetAlbumNavigator /> */}
        {/* <View style={styles.buttonHolderStyle}>
          <CustomButton
            title={PET_PICTURES_TAB_TITLE_TEXT}
            style={styles.buttonSendStyle}
            fontStyle={styles.buttonSendFontStyle}
            onPress={onCallToOwnerClickHandler}
          />
          <CustomButton
            title={PET_VIDEOS_TAB_TITLE_TEXT}
            style={styles.buttonSendStyle}
            fontStyle={styles.buttonSendFontStyle}
            onPress={onDeletePetClickHandler}
          />
        </View>

        <ScrollView>
          <View style={styles.imageViewHolderStyle}>
            <Image
              style={styles.imageViewStyle}
              source={petDetails.petProfilePIC}
            />
          </View>
        </ScrollView> */}
        <ScrollView>
          <View style={styles.imageViewHolderStyle}>
            {/* <Image
              style={styles.imageViewStyle}
              source={petDetails.petProfilePIC}
            /> */}
            {items.map((item) => {
              return (
                <View
                  style={{
                    flexDirection: 'row',
                    margin: 20,
                    justifyContent: 'space-between',
                  }}>
                  <Image
                    style={{width: 100, height: 100}}
                    source={{
                      uri: item,
                    }}
                  />
                </View>
              );
            })}
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    // petDetails: state.PetDetailReducer.petDetails,
    serviceState: state.PetDetailReducer.serviceState,
    loaderVisibility: state.PetDetailReducer.loaderVisibility,
    errorMessage: state.PetDetailReducer.errorMessage,
    successMessage: state.PetDetailReducer.successMessage,
    formErrorMessage: state.PetDetailReducer.formErrorMessage,
  };
};

const PetAlbumPage = PetAlbum_Page; //connect(mapStateToProps)(PetAlbum_Page);

export default PetAlbumPage;

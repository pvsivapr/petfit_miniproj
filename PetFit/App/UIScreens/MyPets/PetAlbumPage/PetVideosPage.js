import React from 'react';
import {
  View,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  Image,
  ScrollView,
} from 'react-native';
import styles from './PetAlbumPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomButton from './../../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
// import {ConsoleLogger} from './../../../BAL/EventLogger';
import {
  PET_ALUBM_PAGE_TITLE,
  PET_PICTURES_TAB_TITLE_TEXT,
  PET_VIDEOS_TAB_TITLE_TEXT,
} from './../../../Constants/TextConstants';

const PetVideos_Page = (props) => {
//   const {route, loaderVisibility} = props;
//   const petDetails = route.params.petDetails;

  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        {/* <View style={styles.mainViewStyle}> */}
        {/* <CustomActivityIndicator visibility={loaderVisibility} /> */}

        <CustomHeader
          title={PET_ALUBM_PAGE_TITLE}
          showNavigationIcon={true}
          // isMasterDetailHeader={true}
          // navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          // onNavigationButtonPress={onNavigationButtonPresshandler}
        />
        <View style={styles.buttonHolderStyle}>
          <CustomButton
            title={PET_PICTURES_TAB_TITLE_TEXT}
            style={styles.buttonSendStyle}
            fontStyle={styles.buttonSendFontStyle}
          />
          <CustomButton
            title={PET_VIDEOS_TAB_TITLE_TEXT}
            style={styles.buttonSendStyle}
            fontStyle={styles.buttonSendFontStyle}
          />
        </View>

        <ScrollView>
          <View style={styles.imageViewHolderStyle}>
            {/* <Image
              style={styles.imageViewStyle}
              source={petDetails.petProfilePIC}
            /> */}
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};

const PetVideosPage = PetVideos_Page; //connect(mapStateToProps)(PetVideos_Page);

export default PetVideosPage;

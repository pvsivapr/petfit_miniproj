import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import AppColors from './../../../Constants/Colors';
import {
  APP_MIN_INPUT_HEIGHT,
  APP_MIN_INPUT_WIDTH,
} from './../../../Constants/PageDimensions';
import {
  TEXT_LINK_COLOR,
  TEXT_INFO_COLOR,
} from './../../../Constants/AppStyleConstants';

const screenHeight = Dimensions.get('screen').height;
const screenWidth = Dimensions.get('screen').width;

const styles = StyleSheet.create({
  textErrorInfoStyle: {
    color: 'maroon',
  },

  editorLegendTextStyle: {
    color: '#000000',
    fontSize: 20,
  },
  editorViewStyle: {
    minHeight: 150,
    height: 150,
    textAlignVertical: 'top',
  },

  buttonSendStyle: {
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
    backgroundColor: '#FFFFFF',
    borderRadius: 0,
    // marginHorizontal: 5,
    // width: (screenWidth / 100) * 50 - 20,
  },
  buttonSendFontStyle: {
    color: '#000000',
  },
  buttonHolderStyle: {
    // flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },

  textInputHolderViewStyle: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
    margin: 10,
    backgroundColor: 'rgb(170,170,170)',
  },

  mainViewStyle: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgb(225,227,230)',
  },
});

export default styles;

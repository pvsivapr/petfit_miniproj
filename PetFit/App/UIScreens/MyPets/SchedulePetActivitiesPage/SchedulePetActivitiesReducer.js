import {
  CHECK_REPORT_ADD_PET_ACTIVITY_FORM_ERROR,
  UPDATE_ADD_PET_ACTIVITY_DETAILS,
  ADD_PET_ACTIVITY_ACCESS,
  ADD_PET_ACTIVITY_SUCCESS_MESSAGE,
  ADD_PET_ACTIVITY_FAILURE_MESSAGE,
  ADD_PET_ACTIVITY_RESET,
} from './SchedulePetActivitiesActions';
import {
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../../Constants/PageNames';

const initialState = {
  petActivityDetails: {
    petId: '',
    activityId: '',
    activityTitle: '',
    activityDateTime: '',
    activityNotes: '',
  },
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  formErrorMessage: '',
};

const SchedulePetActivitiesReducer = (state = initialState, action) => {
  state = {
    ...state,
    successMessage: '',
    errorMessage: '',
    formErrorMessage: '',
  };
  switch (action.type) {
    case CHECK_REPORT_ADD_PET_ACTIVITY_FORM_ERROR:
      state = {
        ...state,
        serviceState: INACTIVE,
        loaderVisibility: false,
        successMessage: '',
        errorMessage: '',
        formErrorMessage: action.payload,
      };
      break;
    case UPDATE_ADD_PET_ACTIVITY_DETAILS:
      state = {
        ...state,
        petActivityDetails: action.payload,
        serviceState: INACTIVE,
        loaderVisibility: false,
        successMessage: '',
        errorMessage: '',
        formErrorMessage: '',
      };
      break;
    case ADD_PET_ACTIVITY_ACCESS:
      state = {...state, serviceState: LOADING, loaderVisibility: true};
      break;
    case ADD_PET_ACTIVITY_SUCCESS_MESSAGE:
      state = {
        ...state,
        serviceState: SUCCESS,
        successMessage: action.payload,
        loaderVisibility: false,
      };
      break;
    case ADD_PET_ACTIVITY_FAILURE_MESSAGE:
      state = {
        ...state,
        serviceState: FAILURE,
        loaderVisibility: false,
        errorMessage: action.payload,
      };
      break;
    case ADD_PET_ACTIVITY_RESET:
      state = {
        ...state,
        petActivityDetails: {
          petId: '',
          activityId: '',
          activityTitle: '',
          activityDateTime: '',
          activityNotes: '',
        },
        serviceState: INACTIVE,
        loaderVisibility: false,
        successMessage: '',
        errorMessage: '',
        formErrorMessage: '',
      };
      break;
  }
  return state;
};

export default SchedulePetActivitiesReducer;

import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Platform,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './SchedulePetActivitiesPageStyles';
import CustomHeader from './../../../CommonComponents/CustomHeader';
import CustomButton from './../../../CommonComponents/CustomButton';
import CustomActivityIndicator from './../../../CommonComponents/CustomActivityIndicator';
import CustomTextInputField from './../../../CommonComponents/CustomTextInputField';
import {ConsoleLogger} from './../../../BAL/EventLogger';
import {PetActivityDetailConstants} from './../../../Constants/PageNames';
import {
  PetActivityDetailTextConstants,
  ADD_PET_ACTIVITY_TITLE,
  ADD_PET_ACTIVITY_BUTTON_TEXT,
} from './../../../Constants/TextConstants';
import {
  fetchCheckReportAddPetActivityFormError,
  fetchUpadteAddPetActivityDetails,
  fetchAddPetActivityAction,
} from './SchedulePetActivitiesActions';

const SchedulePetActivities_Page = (props) => {
  const {
    route,
    dispatch,
    petActivityDetails,
    navigation,
    loaderVisibility,
    formErrorMessage,
  } = props;
  const petId = route.params.petId;

  const onNavigationButtonPresshandler = () => {
    navigation.pop();
  };

  const onPressAddPet = async () => {
    if (petActivityDetails !== null && petActivityDetails !== undefined) {
      if (
        petActivityDetails.activityTitle === null ||
        petActivityDetails.activityTitle === undefined ||
        petActivityDetails.activityTitle === ''
      ) {
        dispatch(
          fetchCheckReportAddPetActivityFormError(
            'Please enter a valid activity name',
          ),
        );
      } else if (
        petActivityDetails.activityDateTime === null ||
        petActivityDetails.activityDateTime === undefined ||
        petActivityDetails.activityDateTime === ''
      ) {
        dispatch(
          fetchCheckReportAddPetActivityFormError(
            'Please enter a valid activity date time',
          ),
        );
      } else if (
        petActivityDetails.activityNotes === null ||
        petActivityDetails.activityNotes === undefined ||
        petActivityDetails.activityNotes === ''
      ) {
        dispatch(
          fetchCheckReportAddPetActivityFormError('Please enter a valid notes'),
        );
      } else {
        dispatch(fetchAddPetActivityAction(petActivityDetails));
      }
    } else {
      dispatch(
        fetchCheckReportAddPetActivityFormError(
          'Please fill all mandatory fields',
        ),
      );
    }
  };

  const onTextChangedHandler = (text, inputID) => {
    let petActivity_Details = petActivityDetails;
    let isupdated = false;
    if (inputID === PetActivityDetailConstants.PET_ACTIVITY_TITLE) {
      petActivity_Details = {...petActivityDetails, activityTitle: text};
      isupdated = true;
    } else if (inputID === PetActivityDetailConstants.PET_ACTIVITY_DATETIME) {
      petActivity_Details = {...petActivityDetails, activityDateTime: text};
      isupdated = true;
    } else if (inputID === PetActivityDetailConstants.PET_ACTIVITY_NOTES) {
      petActivity_Details = {...petActivityDetails, activityNotes: text};
      isupdated = true;
    } else {
      petActivity_Details = {...petActivityDetails};
    }
    if (isupdated) {
      petActivity_Details = {
        ...petActivity_Details,
        petId: petId,
      };
      dispatch(fetchUpadteAddPetActivityDetails(petActivity_Details));
    }
  };

  const onCameraVideoIconClickHandler = () => {};

  const uiMainview = (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.mainViewStyle}>
      <SafeAreaView style={styles.mainViewStyle}>
        {/* <View style={styles.mainViewStyle}> */}
        <CustomActivityIndicator visibility={loaderVisibility} />

        <CustomHeader
          title={ADD_PET_ACTIVITY_TITLE}
          showNavigationIcon={true}
          isMasterDetailHeader={false}
          //   navigationIconSource={MASTER_HEADER_HAM_BURGER}
          navigation={props.navigation}
          onNavigationButtonPress={onNavigationButtonPresshandler}
        />

        <ScrollView>
          <View style={styles.textInputHolderViewStyle}>
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={PetActivityDetailConstants.PET_ACTIVITY_TITLE}
              isFullEntry={false}
              legendTitle={
                PetActivityDetailTextConstants.PET_ACTIVITY_TITLE_LEGEND_TEXT
              }
              placeHolder={
                PetActivityDetailTextConstants.PET_ACTIVITY_TITLE_PLACEHOLDER_TEXT
              }
              hintText={
                PetActivityDetailTextConstants.PET_ACTIVITY_TITLE_ERROR_HINT_TEXT
              }
              value={petActivityDetails.activityTitle}
            />
            <CustomTextInputField
              onChangeText={onTextChangedHandler}
              inputID={PetActivityDetailConstants.PET_ACTIVITY_DATETIME}
              isFullEntry={false}
              legendTitle={
                PetActivityDetailTextConstants.PET_ACTIVITY_TIME_LEGEND_TEXT
              }
              placeHolder={
                PetActivityDetailTextConstants.PET_ACTIVITY_TIME_PLACEHOLDER_TEXT
              }
              hintText={
                PetActivityDetailTextConstants.PET_ACTIVITY_TIME_ERROR_HINT_TEXT
              }
              value={petActivityDetails.activityDateTime}
            />
            <Text style={styles.editorLegendTextStyle}>
              {PetActivityDetailTextConstants.PET_ACTIVITY_NOTES_LEGEND_TEXT}
            </Text>
            <CustomTextInputField
              multiline={true}
              fontStyle={styles.editorViewStyle}
              onChangeText={onTextChangedHandler}
              inputID={PetActivityDetailConstants.PET_ACTIVITY_NOTES}
              legendTitle={
                PetActivityDetailTextConstants.PET_ACTIVITY_NOTES_LEGEND_TEXT
              }
              placeHolder={
                PetActivityDetailTextConstants.PET_ACTIVITY_NOTES_PLACEHOLDER_TEXT
              }
              hintText={
                PetActivityDetailTextConstants.PET_ACTIVITY_NOTES_ERROR_HINT_TEXT
              }
              value={petActivityDetails.activityNotes}
            />
            {formErrorMessage !== null &&
            formErrorMessage !== undefined &&
            formErrorMessage !== '' ? (
              <Text style={styles.textErrorInfoStyle}>{formErrorMessage}</Text>
            ) : (
              <></>
            )}
            <View style={styles.buttonHolderStyle}>
              <CustomButton
                title={ADD_PET_ACTIVITY_BUTTON_TEXT}
                style={styles.buttonSendStyle}
                fontStyle={styles.buttonSendFontStyle}
                onPress={onPressAddPet}
              />
            </View>
          </View>
        </ScrollView>
        {/* </View> */}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
  return uiMainview;
};
const mapStateToProps = (state) => {
  return {
    petActivityDetails: state.SchedulePetActivitiesReducer.petActivityDetails,
    serviceState: state.SchedulePetActivitiesReducer.serviceState,
    loaderVisibility: state.SchedulePetActivitiesReducer.loaderVisibility,
    errorMessage: state.SchedulePetActivitiesReducer.errorMessage,
    successMessage: state.SchedulePetActivitiesReducer.successMessage,
    formErrorMessage: state.SchedulePetActivitiesReducer.formErrorMessage,
  };
};

const SchedulePetActivitiesPage = connect(mapStateToProps)(
  SchedulePetActivities_Page,
);

export default SchedulePetActivitiesPage;

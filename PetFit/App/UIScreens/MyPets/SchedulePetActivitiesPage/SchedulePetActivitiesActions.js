import {ConsoleLogger, ErrorEventLogger} from '../../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../../BAL/LocalDB';
import {PET_DETAILS} from '../../../BAL/LocalDB/LocalDBConstants';
import {getDyanmicIDFromDate} from '../../../CommonComponents/GetDynamicID';
import {fetchGetPetsAction} from '../../HomePage/HomeActions';

export const CHECK_REPORT_ADD_PET_ACTIVITY_FORM_ERROR =
  'CHECK_REPORT_ADD_PET_ACTIVITY_FORM_ERROR';
export const UPDATE_ADD_PET_ACTIVITY_DETAILS =
  'UPDATE_ADD_PET_ACTIVITY_DETAILS';
export const ADD_PET_ACTIVITY_ACCESS = 'ADD_PET_ACTIVITY_ACCESS';
export const ADD_PET_ACTIVITY_SUCCESS_MESSAGE =
  'ADD_PET_ACTIVITY_SUCCESS_MESSAGE';
export const ADD_PET_ACTIVITY_FAILURE_MESSAGE =
  'ADD_PET_ACTIVITY_FAILURE_MESSAGE';
export const ADD_PET_ACTIVITY_RESET = 'ADD_PRODUCT_RESET';

export const fetchCheckReportAddPetActivityFormError = (formErrorMessage) => ({
  type: CHECK_REPORT_ADD_PET_ACTIVITY_FORM_ERROR,
  payload: formErrorMessage,
});
export const fetchUpadteAddPetActivityDetails = (petDetails) => ({
  type: UPDATE_ADD_PET_ACTIVITY_DETAILS,
  payload: petDetails,
});

export const fetchAddPetActivityReset = () => ({
  type: ADD_PET_ACTIVITY_RESET,
});

export const getAddPetActivityAccess = () => ({
  type: ADD_PET_ACTIVITY_ACCESS,
});
export const getAddPetActivitySuccess = (successData) => ({
  type: ADD_PET_ACTIVITY_SUCCESS_MESSAGE,
  payload: 'ADD_PET_ACTIVITY_SUCCESS_MESSAGE',
});
export const getAddPetActivityFailure = (failureReport) => ({
  type: ADD_PET_ACTIVITY_FAILURE_MESSAGE,
  payload: 'ADD_PET_ACTIVITY_FAILURE_MESSAGE',
});

export function fetchAddPetActivityAction(petActivityDetails) {
  return async (dispatchAddPetActivity) => {
    dispatchAddPetActivity(getAddPetActivityAccess());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PET_DETAILS,
        (success_Data) => {
          let available_Pets_Data = null;
          if (success_Data !== null && success_Data.response_data !== null) {
            available_Pets_Data = JSON.parse(success_Data.response_data);
            const itemIndex = available_Pets_Data.findIndex(
              (item) => item.petId === petActivityDetails.petId,
            );

            if (itemIndex !== -1) {
              let obtainedPetItem = available_Pets_Data[itemIndex];
              /**/
              petActivityDetails = {
                ...petActivityDetails,
                activityId: getDyanmicIDFromDate(),
              };
              obtainedPetItem.petScheduleDetails.push(petActivityDetails);
              available_Pets_Data[itemIndex] = obtainedPetItem;
              /**/
              // for (var i = 0; i < 5; i++) {
              //   petActivityDetails = {
              //     ...petActivityDetails,
              //     activityName: getDyanmicIDFromDate() + '____' + i.toString(),
              //     activityId: getDyanmicIDFromDate() + i.toString(),
              //   };
              //   obtainedPetItem.petScheduleDetails.push(petActivityDetails);
              //   available_Pets_Data[itemIndex] = obtainedPetItem;
              // }
              /**/

              // ConsoleLogger(
              //   'SchedulePetActivitiesActions >>>> fetchAddPetActivityAction >>>> obtainedPetItem',
              //   obtainedPetItem,
              // );
              // dispatchAddPetActivity(getAddPetActivityFailure('failureData'));

              CustomAsyncStorage.addDataToStorage(
                PET_DETAILS,
                JSON.stringify(available_Pets_Data),
                (successData) => {
                  dispatchAddPetActivity(
                    getAddPetActivitySuccess('Data added successfully'),
                  );
                  dispatchAddPetActivity(fetchAddPetActivityReset());
                  dispatchAddPetActivity(fetchGetPetsAction());
                },
                (failureData) => {
                  dispatchAddPetActivity(getAddPetActivityFailure(failureData));
                },
              );
            }
          }
        },
        (failureData) => {
          dispatchAddPetActivity(getAddPetActivityFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchAddPetActivity(
        getAddPetActivityFailure({errorMessage: 'Catch Block triggered'}),
      );
    }
  };
}

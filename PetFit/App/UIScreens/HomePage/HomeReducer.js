import {
  GET_PETS,
  GET_PETS_SUCCESS,
  GET_PETS_FAILURE,
  GET_PETS_RESET,
  // PET_ITEM_SELECTED_FROM_LIST,
  // RESET_PET_ITEM_SELECTED_FROM_LIST,
} from './HomeActions';
import {
  RESET,
  FAILURE,
  INACTIVE,
  LOADING,
  SUCCESS,
} from './../../Constants/PageNames';
import {ConsoleLogger} from './../../BAL/EventLogger';

const initialState = {
  petsAdded: [
    // {
    //   petId: '',
    //   petName: '',
    //   petDOB: '',
    //   petAge: '0',
    //   petLocation: '',
    //   petBreed: '',
    //   petOwnerDetails: '',
    //   petImages: [{}],
    //   petVideos: [{}],
    // },
  ],
  serviceState: INACTIVE,
  loaderVisibility: false,
  successMessage: '',
  errorMessage: '',
  shallOpenCamera: false,
  selectedPetDetails: null,
  // navigation: {},
  // formErrorMessage: '',
};

const HomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PETS:
      state = {
        ...state,
        serviceState: LOADING,
        loaderVisibility: true,
      };
      break;
    case GET_PETS_SUCCESS:
      state = {
        ...state,
        petsAdded: action.payload,
        serviceState: SUCCESS,
        loaderVisibility: false,
      };
      break;
    case GET_PETS_FAILURE:
      state = {
        ...state,
        serviceState: FAILURE,
        loaderVisibility: false,
      };
      break;
    case GET_PETS_RESET:
      state = {
        ...state,
        serviceState: RESET,
        loaderVisibility: false,
      };
      break;
    // case PET_ITEM_SELECTED_FROM_LIST:
    //   state = {
    //     ...state,
    //     selectedPetDetails: action.payload,
    //     serviceState: INACTIVE,
    //     loaderVisibility: false,
    //   };
    //   break;
    // case RESET_PET_ITEM_SELECTED_FROM_LIST:
    //   state = {...state, serviceState: INACTIVE, loaderVisibility: false};
    //   break;
  }
  return state;
};

export default HomeReducer;

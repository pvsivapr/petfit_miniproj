import React from 'react';
import {View, Text, Image, FlatList, SafeAreaView} from 'react-native';
import styles from './HomePageStyles';
import CustomHeader from './../../CommonComponents/CustomHeader';
import CustomActivityIndicator from './../../CommonComponents/CustomActivityIndicator';
import {PET_DETAIL_PAGE} from './../../Constants/PageNames';
import CustomCarouselView from '../../CommonComponents/CustomCarouselView';
import {
  MASTER_HEADER_HAM_BURGER,
  PET_PIC_PLACEHOLDER,
} from './../../Assets/ImageHelper';
import {connect} from 'react-redux';
import CustomTouch from '../../CommonComponents/CustomTouch';

import {fetchGetPetsAction, fetchGetPetsReset} from './HomeActions';

class Home_Page extends React.Component {
  onItemSelectedHandler(item) {
    const {dispatch, navigation, petDetails} = item;
    dispatch(fetchGetPetsReset());
    navigation.navigate(PET_DETAIL_PAGE, {
      petDetails: petDetails,
    });
  }

  componentDidMount() {
    const {dispatch, navigation} = this.props;
    navigation.closeDrawer();
    dispatch(fetchGetPetsAction());
  }

  carouselItemView = ({item}) => {
    const customItem = {
      petDetails: item,
      navigation: this.props.navigation,
      dispatch: this.props.dispatch,
    };
    return (
      <CustomTouch childData={customItem} onPress={this.onItemSelectedHandler}>
        <View style={styles.carouselViewHolderStyle}>
          {item.petProfilePIC !== null &&
          item.petProfilePIC !== undefined &&
          item.petProfilePIC !== '' ? (
            <Image
              style={styles.carouselItemImageStyle}
              source={{uri: item.petProfilePIC}}
            />
          ) : (
            <>
              <Image
                style={styles.carouselItemImageStyle}
                source={PET_PIC_PLACEHOLDER}
              />
              <Text style={{position: 'absolute', bottom: 20, fontSize: 20}}>
                {item.petName}
              </Text>
            </>
          )}
        </View>
      </CustomTouch>
    );
  };

  flatListItemView = ({item}) => {
    const customItem = {
      petDetails: item,
      navigation: this.props.navigation,
      dispatch: this.props.dispatch,
    };
    return (
      <CustomTouch childData={customItem} onPress={this.onItemSelectedHandler}>
        <View style={styles.flatListHolderStyle}>
          {item.petProfilePIC !== null &&
          item.petProfilePIC !== undefined &&
          item.petProfilePIC !== '' ? (
            <Image
              style={styles.flatListItemImageStyle}
              source={{uri: item.petProfilePIC}}
            />
          ) : (
            <>
              <Image
                style={styles.flatListItemImageStyle}
                source={PET_PIC_PLACEHOLDER}
              />
            </>
          )}
          <View style={styles.flatListItemTextHolderStyle}>
            <Text
              style={
                styles.flatListItemTextStyle
              }>{`Pet Name: ${item.petName}`}</Text>
            <Text
              style={
                styles.flatListItemTextStyle
              }>{`Pet Age: ${item.petAge}`}</Text>
            <Text
              style={
                styles.flatListItemTextStyle
              }>{`Pet Location: ${item.petLocation}`}</Text>
            <Text
              style={
                styles.flatListItemTextStyle
              }>{`Pet Breed: ${item.petBreed}`}</Text>
          </View>
        </View>
      </CustomTouch>
    );
  };

  render() {
    const {petsAdded, loaderVisibility} = this.props;
    const uiMain = (
      <SafeAreaView style={styles.mainViewStyle}>
        <View style={styles.mainViewStyle}>
          <CustomActivityIndicator visibility={loaderVisibility} />
          <CustomHeader
            title={'Home Page'}
            showNavigationIcon={true}
            isMasterDetailHeader={true}
            navigationIconSource={MASTER_HEADER_HAM_BURGER}
            navigation={this.props.navigation}
          />
          {petsAdded !== null && petsAdded !== undefined ? (
            <>
              <View style={styles.carouselViewContainerStyle}>
                <CustomCarouselView
                  itemsList={petsAdded}
                  itemView={this.carouselItemView}
                />
              </View>
              <View style={styles.flatListContainerStyle}>
                <FlatList
                  data={petsAdded}
                  renderItem={this.flatListItemView}
                  keyExtractor={(item) => item.id}
                />
              </View>
            </>
          ) : (
            <></>
          )}
        </View>
      </SafeAreaView>
    );
    return uiMain;
  }
}

const mapStateToProps = (state) => {
  return {
    petsAdded: state.HomeReducer.petsAdded,
    serviceState: state.HomeReducer.serviceState,
    loaderVisibility: state.HomeReducer.loaderVisibility,
    errorMessage: state.HomeReducer.errorMessage,
    successMessage: state.HomeReducer.successMessage,
    shallOpenCamera: state.HomeReducer.shallOpenCamera,
    selectedPetDetails: state.HomeReducer.selectedPetDetails,
  };
};

const HomePage = connect(mapStateToProps)(Home_Page);
export default HomePage;

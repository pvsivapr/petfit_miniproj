import {ErrorEventLogger} from '../../BAL/EventLogger';
import {CustomAsyncStorage} from '../../BAL/LocalDB';
import {PET_DETAILS} from '../../BAL/LocalDB/LocalDBConstants';

export const GET_PETS = 'GET_PETS';
export const GET_PETS_SUCCESS = 'GET_PETS_SUCCESS';
export const GET_PETS_FAILURE = 'GET_PETS_FAILURE';
export const GET_PETS_RESET = 'GET_PETS_RESET';
// export const PET_ITEM_SELECTED_FROM_LIST = 'PET_ITEM_SELECTED_FROM_LIST';
// export const RESET_PET_ITEM_SELECTED_FROM_LIST =
//   'RESET_PET_ITEM_SELECTED_FROM_LIST';

export const fetchGetPetsReset = () => ({
  type: GET_PETS_RESET,
});

export const getPets = () => ({
  type: GET_PETS,
});

export const getPetsSuccess = (available_Pets_Data) => ({
  type: GET_PETS_SUCCESS,
  payload: available_Pets_Data,
});

export const getPetsFailure = (failureData) => ({
  type: GET_PETS_FAILURE,
  payload: failureData,
});

export function fetchGetPetsAction() {
  return async (dispatchViewPets) => {
    dispatchViewPets(getPets());
    try {
      CustomAsyncStorage.getDataFromStorage(
        PET_DETAILS,
        (success_Data) => {
          const available_Pets_Data = JSON.parse(success_Data.response_data);
          dispatchViewPets(getPetsSuccess(available_Pets_Data));
        },
        (failureData) => {
          dispatchViewPets(getPetsFailure(failureData));
        },
      );
    } catch (error) {
      ErrorEventLogger(error);
      dispatchViewPets(getPetsFailure({errorMessage: 'Catch Block triggered'}));
    }
  };
}

// export const fetchSelectedPetDetails = (selectedPetDetails) => ({
//   type: PET_ITEM_SELECTED_FROM_LIST,
//   payload: selectedPetDetails,
// });

// export const fetchResetSelectedPetDetails = () => ({
//   type: RESET_PET_ITEM_SELECTED_FROM_LIST,
// });

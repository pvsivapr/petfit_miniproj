import {Dimensions, StyleSheet} from 'react-native';
import {
  APP_MIN_INPUT_HEIGHT,
  APP_MIN_INPUT_WIDTH,
} from './../../Constants/PageDimensions';

const SLIDER_WIDTH = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.7);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 4);

const styles = StyleSheet.create({
  carouselItemImageStyle: {
    width: '100%',
    height: undefined,
    aspectRatio: 1,
  },
  carouselViewHolderStyle: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'dodgerblue',
    margin: 5,
  },
  carouselViewContainerStyle: {
    margin: 10,
  },

  flatListItemImageStyle: {
    width: '25%',
    height: undefined,
    aspectRatio: 1,
  },
  flatListItemTextStyle: {
    color: '#000000',
    marginBottom: 5,
  },
  flatListItemTextHolderStyle: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 10,
  },
  flatListHolderStyle: {
    backgroundColor: 'rgb(172,172,172)',
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  flatListContainerStyle: {
    margin: 10,
    marginBottom: (windowHeight / 100) * 35,
  },

  mainViewStyle: {
    flex: 1,
    backgroundColor: 'rgb(225,227,230)',
    paddingTop: 0,
  },
  buttonHomeStyle: {
    minWidth: APP_MIN_INPUT_WIDTH,
    minHeight: APP_MIN_INPUT_HEIGHT,
  },
});

export default styles;

export const DateManipulations = {
  getDay: function (date = new Date()) {
    const currentDayDate = date.getDate();
    return currentDayDate;
  },
  getMonth: function (date = new Date()) {
    const currentMonthNumber = date.getMonth() + 1;
    let currentMonthObject = {};
    monthsObject.forEach((monthItem) => {
      if (currentMonthNumber.toString() === monthItem.id.toString()) {
        currentMonthObject = monthItem;
      }
    });
    return currentMonthObject?.name;
    // return currentMonthObject?.code;
    // return currentMonthObject?.monthNumber;
  },
  getYear: function (date = new Date()) {
    const currentYear = date.getFullYear();
    return currentYear;
  },
  getTicks: function (date = new Date()) {
    const todayDate = date; //new Date(); // for example
    // the number of .net ticks at the unix epoch
    const epochTicks = 621355968000000000;
    // there are 10000 .net ticks per millisecond
    const ticksPerMillisecond = 10000;
    // calculate the total number of .net ticks for your date
    const todayDateTicks =
      epochTicks + todayDate.getTime() * ticksPerMillisecond;
    return todayDateTicks;
  },
  getTimeByFormat: function (date = new Date()) {
    const hours = new Date().getHours(); //Current Hours
    const minutes = new Date().getMinutes(); //Current Minutes
    const seconds = new Date().getSeconds(); //Current Seconds
    let timeOfDate = hours + ' : ' + minutes + ' : ' + seconds;
    return timeOfDate;
  },
  getDateByFormat: function (date = new Date()) {},
};

const monthsObject = [
  {
    id: 1,
    name: 'January',
    code: 'Jan',
    monthNumber: '01',
  },
  {
    id: 2,
    name: 'Feburary',
    code: 'Feb',
    monthNumber: '02',
  },
  {
    id: 3,
    name: 'March',
    code: 'Mar',
    monthNumber: '03',
  },
  {
    id: 4,
    name: 'April',
    code: 'Apr',
    monthNumber: '04',
  },
  {
    id: 5,
    name: 'May',
    code: 'May',
    monthNumber: '05',
  },
  {
    id: 6,
    name: 'June',
    code: 'Jun',
    monthNumber: '06',
  },
  {
    id: 7,
    name: 'July',
    code: 'Jul',
    monthNumber: '07',
  },
  {
    id: 8,
    name: 'August',
    code: 'Aug',
    monthNumber: '08',
  },
  {
    id: 9,
    name: 'September',
    code: 'Sep',
    monthNumber: '09',
  },
  {
    id: 10,
    name: 'October',
    code: 'Oct',
    monthNumber: '10',
  },
  {
    id: 11,
    name: 'November',
    code: 'Nov',
    monthNumber: '11',
  },
  {
    id: 12,
    name: 'December',
    code: 'Dec',
    monthNumber: '12',
  },
];

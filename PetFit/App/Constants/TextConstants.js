export const CANCEL_BUTTON_TEXT = 'Cancel';

export const USER_LOGIN_SUCCESS_MESSAGE = 'User Logged in successfully';
export const USER_LOGIN_FAILURE_MESSAGE = 'Unable to login user';

export const USER_REGISTER_SUCCESS_MESSAGE = 'User Registered successfully';
export const USER_REGISTER_FAILURE_MESSAGE = 'Unable to register user';

export const UserCredentialTextConstants = {
  USER_NAME_PLACEHOLDER_TEXT: 'Enter User Name',
  USER_NAME_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',
  USER_PASSWORD_PLACEHOLDER_TEXT: 'Enter User Password',
  USER_PASSWORD_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',
  USER_FNAME_PLACEHOLDER_TEXT: 'Enter User First Name',
  USER_FNAME_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',
  USER_LNAME_PLACEHOLDER_TEXT: 'Enter User Last Name',
  USER_LNAME_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',
  USER_EMAIL_PLACEHOLDER_TEXT: 'Enter User Email Id',
  USER_EMAIL_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',
  USER_MOBILE_PLACEHOLDER_TEXT: 'Enter User Mobile Number',
  USER_MOBILE_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',
};

export const PetDetailTextConstants = {
  PET_NAME_PLACEHOLDER_TEXT: 'Enter Pet Name',
  PET_NAME_LEGEND_TEXT: 'Pet Name:',
  PET_NAME_ERROR_HINT_TEXT: 'Please enter you pet name',

  PET_DOB_PLACEHOLDER_TEXT: 'Date of Birth',
  PET_DOB_LEGEND_TEXT: 'Pet DOB:',
  PET_DOB_ERROR_HINT_TEXT:
    'Please enter the specie name or the type of pet you have',

  PET_LOCATION_PLACEHOLDER_TEXT: 'Enter Pet Location',
  PET_LOCATION_LEGEND_TEXT: 'Pet Location:',
  PET_LOCATION_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',

  PET_BREED_PLACEHOLDER_TEXT: 'Enter Pet Breed',
  PET_BREED_LEGEND_TEXT: 'Pet Breed:',
  PET_BREED_ERROR_HINT_TEXT: "Please enter you pet's breed name",

  PET_OWNER_DETAILS_PLACEHOLDER_TEXT: 'Enter Owner Details',
  PET_OWNER_DETAILS_LEGEND_TEXT: 'Owner Details:',
  PET_OWNER_DETAILS_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',
};

export const PetActivityDetailTextConstants = {
  PET_ACTIVITY_TITLE_PLACEHOLDER_TEXT: 'Enter Activity',
  PET_ACTIVITY_TITLE_LEGEND_TEXT: 'Activity :',
  PET_ACTIVITY_TITLE_ERROR_HINT_TEXT: 'Please enter activity name',

  PET_ACTIVITY_TIME_PLACEHOLDER_TEXT: 'When',
  PET_ACTIVITY_TIME_LEGEND_TEXT: 'When :',
  PET_ACTIVITY_TIME_ERROR_HINT_TEXT: 'Please enter the activity date time',

  PET_ACTIVITY_NOTES_PLACEHOLDER_TEXT: 'Notes',
  PET_ACTIVITY_NOTES_LEGEND_TEXT: 'Notes',
  PET_ACTIVITY_NOTES_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid Notes',
};

export const ProductDetailTextConstants = {
  PRODUCT_NAME_PLACEHOLDER_TEXT: 'Enter Name',
  PRODUCT_NAME_LEGEND_TEXT: 'Product \n Name:',
  PRODUCT_NAME_ERROR_HINT_TEXT: 'Please enter you product name',

  PRODUCT_COST_PLACEHOLDER_TEXT: 'Enter cost',
  PRODUCT_COST_LEGEND_TEXT: 'Product \n cost:',
  PRODUCT_COST_ERROR_HINT_TEXT:
    'Please enter the specie name or the type of PRODUCT you have',

  PRODUCT_IMAGE_PLACEHOLDER_TEXT: 'Enter Url',
  PRODUCT_IMAGE_LEGEND_TEXT: 'Product \n Image:',
  PRODUCT_IMAGE_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',

  PRODUCT_AVAILABLE_STOCK_PLACEHOLDER_TEXT: 'Enter stock',
  PRODUCT_AVAILABLE_STOCK_LEGEND_TEXT: 'Available \n stock:',
  PRODUCT_AVAILABLE_STOCK_ERROR_HINT_TEXT:
    "Please enter you PRODUCT's breed name",
};

export const FeedbackDetailTextConstants = {
  PRODUCT_NAME_PLACEHOLDER_TEXT: 'Enter Name',
  PRODUCT_NAME_LEGEND_TEXT: 'Product \n Name:',
  PRODUCT_NAME_ERROR_HINT_TEXT: 'Please enter you product name',

  PRODUCT_COST_PLACEHOLDER_TEXT: 'Enter cost',
  PRODUCT_COST_LEGEND_TEXT: 'Product \n cost:',
  PRODUCT_COST_ERROR_HINT_TEXT:
    'Please enter the specie name or the type of PRODUCT you have',

  PRODUCT_IMAGE_PLACEHOLDER_TEXT: 'Enter Url',
  PRODUCT_IMAGE_LEGEND_TEXT: 'Product \n Image:',
  PRODUCT_IMAGE_ERROR_HINT_TEXT:
    'This is a mandatory field, please fill a valid input',

  PRODUCT_AVAILABLE_STOCK_PLACEHOLDER_TEXT: 'Enter stock',
  PRODUCT_AVAILABLE_STOCK_LEGEND_TEXT: 'Available \n stock:',
  PRODUCT_AVAILABLE_STOCK_ERROR_HINT_TEXT:
    "Please enter you PRODUCT's breed name",
};

export const ADD_PET_SUCCESS_MESSAGE =
  'Pet is added to the database successfully';
export const ADD_PET_FAILURE_MESSAGE = 'Unable to add pet to the database';
export const ADD_PET_TITLE = 'Add Pet';
export const ADD_PET_BUTTON_TEXT = 'ADD PET';

export const ADD_PET_ACTIVITY_TITLE = "Schedule Pet's Activities";
export const ADD_PET_ACTIVITY_BUTTON_TEXT = 'Add Activity';

export const PET_DETAIL_TITLE = 'Pet Detail';
export const CALL_OWNER_BUTTON_TEXT = 'Call to Owner';
export const DELETE_PET_BUTTON_TEXT = 'Delete';

export const PET_ALUBM_PAGE_TITLE = 'Pets Album';
export const PET_PICTURES_TAB_TITLE_TEXT = 'PHOTOS';
export const PET_VIDEOS_TAB_TITLE_TEXT = 'VIDEOS';

export const ADD_PRODUCT_PAGE_TITLE = 'Add Product';
export const ADD_PRODUCT_ADD_BUTTON_TEXT = 'ADD';

export const VIEW_PRODUCTS_PAGE_TITLE = 'View Products';

export const VIEW_FEEDBACK_PAGE_TITLE = 'View Feedback';

export const addFeedbackPageTextConstants = {
  ADD_FEEDBACK_PAGE_TITLE: 'Add Feedback',
  ADD_FEEDBACK_PAGE_HEADER_TEXT: 'Welcome New user',
  ADD_FEEDBACK_PAGE_INFO_TEXT_1:
    'we would like your feedback to improve our app',
  ADD_FEEDBACK_PAGE_INFO_TEXT_2:
    'How do you feel about the features of the app',
  ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_1: 'Very dissatisfied',
  ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_2: 'Dissatisfied',
  ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_3: 'Average',
  ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_4: 'Satisfied',
  ADD_FEEDBACK_PAGE_CATEGORY_LEVEL_5: 'Very satisfied',
  ADD_FEEDBACK_PAGE_CATEGORY_HEADER: 'Please select any feedback category',
  ADD_FEEDBACK_PAGE_CATEGORY_TYPE_1: 'SUGGESTIONS',
  ADD_FEEDBACK_PAGE_CATEGORY_TYPE_2: 'COMPLAINTS',
  ADD_FEEDBACK_PAGE_CATEGORY_TYPE_3: 'COMPLEMENTS',
  ADD_FEEDBACK_BUTTON_TEXT: 'Send',
  ADD_FEEDBACK_NOTES_LEGEND_TEXT: 'Please leave your feedback below',
  ADD_FEEDBACK_NOTES_PLACEHOLDER_TEXT: 'Feedback',
  ADD_FEEDBACK_NOTES_ERROR_HINT_TEXT: 'Please enter the feedback',
};

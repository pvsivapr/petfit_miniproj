import React, {useState} from 'react';

import {ActivityIndicator} from 'react-native';
import CustomModal from './CustomModal';

const CustomActivityIndicator = (props) => {
  // const [loadingText, setLoadingText] = useState("loading");
  const [] = useState(props.visibility);
  const {visibility} = props;

  const mainUIComponent = (
    <CustomModal visible={visibility}>
      <ActivityIndicator size="large" color="#0000ff" />
    </CustomModal>
  );
  return mainUIComponent;
};

export default CustomActivityIndicator;

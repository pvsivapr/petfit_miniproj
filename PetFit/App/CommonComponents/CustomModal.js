import React from 'react';
import {
  Modal,
  View,
  TouchableOpacity,
  StyleSheet,
  Keyboard,
  KeyboardAvoidingView,
  Dimensions,
} from 'react-native';

const CustomModal = (props) => {
  const {
    visible,
    style,
    onRequestClose,
    animationType,
    transparent,
    children,
    onPress,
  } = props;

  const mainUIComponent = (
    <Modal
      animationType={animationType ?? 'slide'}
      transparent={transparent ?? true}
      visible={visible}
      onRequestClose={onRequestClose}>
      <TouchableOpacity
        accessibility="none"
        accessibile={false}
        importantForAccessibility="no"
        accessibilityHint=""
        activeOpacity={1}
        onPress={() => {
          Keyboard.dismiss();
          if (onPress && onPress !== null && onPress !== undefined) {
            onPress();
          }
        }}>
        <View style={{...styles.actionSheetMainHolderStyle, ...style}}>
          <KeyboardAvoidingView
            style={styles.keyboardAvoidingViewStyle}
            behavior="padding">
            {children}
          </KeyboardAvoidingView>
        </View>
      </TouchableOpacity>
    </Modal>
  );

  return mainUIComponent;
};

const {height} = Dimensions.get('window');

const styles = StyleSheet.create({
  actionSheetMainHolderStyle: {
    overflow: 'hidden',
    height,
    backgroundColor: '#00000040',
    justifyContent: 'flex-end',
  },
  keyboardAvoidingViewStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default CustomModal;

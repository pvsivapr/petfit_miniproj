import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

class CustomTextField extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      style,
      fontStyle,
      legendTextStyle,
      numberOfLines,
      legendTitle,
      value,
      isFullDisplay,
    } = this.props;

    const uiMainComponent = (
      <>
        {isFullDisplay === false ? (
          <View style={styles.mainHolderContainerStyle}>
            <Text style={styles.textStyle}>{legendTitle}</Text>
            <View style={{...styles.mainViewStyle, ...style, flex: 1}}>
              <Text
                style={{...styles.textDataStyle, ...fontStyle}}
                numberOfLines={numberOfLines}>
                {value}
              </Text>
            </View>
          </View>
        ) : (
          <View style={{...styles.mainViewStyle, ...style}}>
            <Text
              style={{...styles.textDataStyle, ...fontStyle}}
              numberOfLines={numberOfLines}>
              {value}
            </Text>
          </View>
        )}
      </>
    );

    return uiMainComponent;
  }
}

const styles = StyleSheet.create({
  textDataStyle: {
    fontSize: 20,
    color: '#000000',
    margin: 10,
  },
  textStyle: {
    color: '#000000',
    fontSize: 20,
    alignSelf: 'center',
    width: '35%',
  },
  mainViewStyle: {},
  mainHolderContainerStyle: {
    flexDirection: 'row',
    alignContent: 'center',
  },
});

export default CustomTextField;

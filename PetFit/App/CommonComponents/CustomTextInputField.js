import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

class CustomTextInputField extends React.Component {
  onChangeTextHandler = (text) => {
    const {onChangeText, inputID} = this.props;
    onChangeText(text, inputID);
  };

  render() {
    const {
      fontStyle,
      maxLength,
      keyboardType,
      placeHolder,
      numberOfLines,
      value,
      multiline,

      style,
      legendTextStyle,
      legendTitle,
      hintText,
      hintTextStyle,
      isError,

      isFullEntry,

      secureTextEntry,
    } = this.props;
    const uiMainComponent = (
      <>
        {isFullEntry === false ? (
          <View style={styles.mainHolderContainerStyle}>
            <Text style={styles.textStyle}>{legendTitle}</Text>
            <View style={{...styles.mainHolderViewStyle, flex: 1}}>
              <TextInput
                style={{...styles.textInputStyle, ...fontStyle}}
                onChangeText={this.onChangeTextHandler}
                onFocus={() => this.setState({mode: this.EDIT_MODE})}
                onBlur={() => this.setState({mode: this.NORMAL_MODE})}
                maxLength={maxLength}
                keyboardType={keyboardType}
                placeholder={placeHolder}
                numberOfLines={numberOfLines}
                multiline={multiline}
                value={value}
                secureTextEntry={secureTextEntry}
              />
            </View>
          </View>
        ) : (
          <View style={styles.mainHolderViewStyle}>
            <TextInput
              style={{...styles.textInputStyle, ...fontStyle}}
              onChangeText={this.onChangeTextHandler}
              onFocus={() => this.setState({mode: this.EDIT_MODE})}
              onBlur={() => this.setState({mode: this.NORMAL_MODE})}
              maxLength={maxLength}
              keyboardType={keyboardType}
              placeholder={placeHolder}
              numberOfLines={numberOfLines}
              multiline={multiline}
              value={value}
              secureTextEntry={secureTextEntry}
            />
          </View>
        )}
      </>
    );
    return uiMainComponent;
  }
}

const styles = StyleSheet.create({
  textStyle: {
    color: '#000000',
    fontSize: 20,
    alignSelf: 'center',
    width: '35%',
  },
  textInputStyle: {
    fontSize: 20,
    color: '#000000',
    marginLeft: 10,
    minHeight: 50,
    flex: 1,
  },
  mainHolderViewStyle: {
    // borderColor: 'black',
    backgroundColor: 'white',
    // borderWidth: 1,
    marginBottom: 10,
    borderRadius: 10,
    flexDirection: 'row',
  },
  mainHolderContainerStyle: {
    flexDirection: 'row',
    alignContent: 'center',
  },
});

export default CustomTextInputField;

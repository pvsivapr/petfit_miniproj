export const getDyanmicIDFromDate = () => {
  const todayDate = new Date(); // for example
  // the number of .net ticks at the unix epoch
  const epochTicks = 621355968000000000;
  // there are 10000 .net ticks per millisecond
  const ticksPerMillisecond = 10000;
  // calculate the total number of .net ticks for your date
  const todayDateTicks = epochTicks + todayDate.getTime() * ticksPerMillisecond;

  return todayDateTicks;
};

export const getAge = (dateString) => {
  // var today = new Date();
  // var birthDate = new Date(dateString);
  // var age = today.getFullYear() - birthDate.getFullYear();
  // var m = today.getMonth() - birthDate.getMonth();
  // if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
  //     age--;
  // }
  // return age;
  var dob = new Date(dateString);
  //calculate month difference from current date in time
  var month_diff = Date.now() - dob.getTime();
  //convert the calculated difference in date format
  var age_dt = new Date(month_diff);
  //extract year from date
  var year = age_dt.getUTCFullYear();
  //now calculate the age of the user
  var age = Math.abs(year - 1970);
  return age;
};

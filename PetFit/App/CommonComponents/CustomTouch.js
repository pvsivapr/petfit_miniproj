import React from 'react';
import {TouchableOpacity} from 'react-native';

const CustomTouch = (props) => {
  let TouchableComponent = TouchableOpacity;

  const {onPress, children, childData} = props;

  const onPressHandler = () => {
    onPress(childData);
  };

  const mainUIComponent = (
    <TouchableComponent onPress={onPressHandler}>{children}</TouchableComponent>
  );
  return mainUIComponent;
};

export default CustomTouch;

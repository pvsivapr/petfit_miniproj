import React from 'react';
import {
  SafeAreaView,
  SafeAreaProvider,
  SafeAreaInsetsContext,
  useSafeAreaInsets,
  initialWindowMetrics,
} from 'react-native-safe-area-context';

import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';

import {RootReducer} from './App/BAL/RootReducer';
import {NavigationContainer} from '@react-navigation/native';
import RootNavigator from './App/Navigation/RootNavigation';

/*

npm install -g ios-deploy;
react-native run-ios --device "Venkata Sivaprasad Reddy’s iPhone";

*/

const store = createStore(RootReducer, applyMiddleware(thunk));
const App = (props) => {
  const appMain = (
    <Provider store={store}>
      <SafeAreaProvider>
        <NavigationContainer>
          <RootNavigator {...props} />
        </NavigationContainer>
      </SafeAreaProvider>
    </Provider>
  );
  return appMain;
};

export default App;
